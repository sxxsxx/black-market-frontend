package com.example.blackmarket.module.QuestionAnswer.Total;

import android.os.Handler;
import android.util.Log;

import com.example.blackmarket.callback.IShopCallback;
import com.example.blackmarket.entities.QuestionList;
import com.example.blackmarket.model.BlackMarketApi;
import com.example.blackmarket.module.Base.BasePresenter;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class QuestionAnswerTotalPresenter extends BasePresenter<QuestionAnswerTotalView> implements IShopCallback {
    private Handler mHandler;
    public QuestionAnswerTotalPresenter(QuestionAnswerTotalView view) {
        attachView(view);
        mHandler=new Handler();
    }

    public void getData(int pageNo,int typeId){
        BlackMarketApi.question_total(pageNo,typeId, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    String data = response.body().string();
                    mHandler.post(() -> onSuccess(data));
                }
                else {
                    assert response.body() != null;
                    String msg = "code: "+ response.code() + " body: " +response.body().string();
                    Log.e("response failure",msg);
                    mHandler.post(()->onFail(msg));
                }
            }
        });
    }

    @Override
    public void onSuccess(Object data) {
        if(isViewAttached()) {
            QuestionList questionList = new Gson().fromJson((String) data, QuestionList.class);
            this.getView().showData(questionList);
        }
    }

    @Override
    public void onFail(String msg) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onComplete() {

    }
}
