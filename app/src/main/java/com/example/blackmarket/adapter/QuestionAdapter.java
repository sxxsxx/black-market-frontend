package com.example.blackmarket.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.example.blackmarket.R;
import com.example.blackmarket.entities.QuestionList;

import java.util.List;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.ViewHolder> {
    private List<QuestionList.Question> dataList;
    private Context context;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(QuestionAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public QuestionAdapter(List<QuestionList.Question> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView question_answer_total_head;
        private TextView question_answer_total_name;
        private TextView question_answer_total_price;
        private TextView question_answer_total_title;
        private TextView question_answer_total_date;
        private LinearLayout linearLayout;//item的layout

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            question_answer_total_head=itemView.findViewById(R.id.question_answer_total_head);
            question_answer_total_name=itemView.findViewById(R.id.question_answer_total_name);
            question_answer_total_price=itemView.findViewById(R.id.question_answer_total_price);
            question_answer_total_title=itemView.findViewById(R.id.question_answer_total_title);
            question_answer_total_date=itemView.findViewById(R.id.question_answer_total_date);
            linearLayout = itemView.findViewById(R.id.question_item_layout);
        }
    }

    @NonNull
    @Override
    public QuestionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.question_answer_total_item, null);
        QuestionAdapter.ViewHolder holder = new QuestionAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionAdapter.ViewHolder holder, final int pos) {
        QuestionList.Question records=dataList.get(pos);
        //获取头像
        Glide.with(context).load(records.getPictureUrl()).apply(RequestOptions.bitmapTransform(new CircleCrop())).into(holder.question_answer_total_head);
        //holder.question_answer_total_head.setImageResource(Integer.parseInt(records.getPictureLocation()));
        //获取昵称、价格、标题、时间
        holder.question_answer_total_name.setText(records.getNickname());
        holder.question_answer_total_price.setText(""+records.getConsultationPrice()+" 黑币");
        holder.question_answer_total_title.setText(records.getConsultationTitle());
        holder.question_answer_total_date.setText(records.getAskTime());

        //为layout添加一个OnClickListener，点击layout时就会调用onClick方法
        if (onItemClickListener!=null){
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //点击后调用主类设置的onItemClickListener，执行主类在其中设置的点击后的动作
                    onItemClickListener.onItemClick(pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataList==null? 0:dataList.size();
    }


}
