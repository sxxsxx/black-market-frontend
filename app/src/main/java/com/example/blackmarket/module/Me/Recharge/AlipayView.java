package com.example.blackmarket.module.Me.Recharge;

import com.example.blackmarket.entities.FailureResponse;

public interface AlipayView {
    void showSuccess(String response);
    void showFailure(FailureResponse failureResponse);
}
