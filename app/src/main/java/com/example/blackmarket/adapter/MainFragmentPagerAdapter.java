package com.example.blackmarket.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.blackmarket.module.Home.HomeFragment;
import com.example.blackmarket.module.Home.MessageFragment;
import com.example.blackmarket.module.Home.WebFragment;
import com.example.blackmarket.module.Me.MeFragment;
import com.example.blackmarket.module.SecondHandTrade.Shoplist.BuyingFragment;
import com.example.blackmarket.module.SecondHandTrade.Shoplist.SellingFragment;

import java.util.List;

public class MainFragmentPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> fragments;
    public MainFragmentPagerAdapter(FragmentManager fm,List<Fragment> fragments ) {
        super(fm);
        this.fragments=fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

}

