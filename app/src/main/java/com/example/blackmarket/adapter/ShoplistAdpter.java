package com.example.blackmarket.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.example.blackmarket.R;
import com.example.blackmarket.entities.SellingShopItem;

import java.util.List;

public class ShoplistAdpter extends RecyclerView.Adapter<ShoplistAdpter.ViewHolder> {
    private List<SellingShopItem.ShopData> dataList;
    private Context context;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    //定义一个设置监听的方法，便于主类设置点击后的动作
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public ShoplistAdpter(List<SellingShopItem.ShopData> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        ViewHolder viewHolder;
        view = LayoutInflater.from(context).inflate(R.layout.item_shoplist, null);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShoplistAdpter.ViewHolder viewHolder, final int pos) {
        //加载商品图片
        String shopImageUri = dataList.get(pos).getProductPictureLocation();
        Glide.with(context).load(shopImageUri).into(viewHolder.shopImage);
        //加载用户头像
        String sellerImageUri = dataList.get(pos).getPictureUrl();
        Glide.with(context).load(sellerImageUri).apply(RequestOptions.bitmapTransform(new CircleCrop())).into(viewHolder.sellerImage);
        //商品名、价格、校区
        viewHolder.shopName.setText(dataList.get(pos).getProductName());
        viewHolder.shopPrice.setText(dataList.get(pos).getProductPrice()+"");
        viewHolder.areaName.setText(dataList.get(pos).getCampus());

        //为layout添加一个OnClickListener，点击layout时就会调用onClick方法
        if (onItemClickListener!=null){
            viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //点击后调用主类设置的onItemClickListener，执行主类在其中设置的点击后的动作
                    onItemClickListener.onItemClick(pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataList==null? 0:dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView shopImage;
        private ImageView sellerImage;
        private TextView shopName;
        private TextView shopPrice;
        private TextView areaName;
        private LinearLayout linearLayout;//item的layout

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            shopImage=itemView.findViewById(R.id.shop_img);
            sellerImage=itemView.findViewById(R.id.user_img);
            shopName=itemView.findViewById(R.id.shop_name);
            shopPrice=itemView.findViewById(R.id.shop_price);
            areaName=itemView.findViewById(R.id.area_name);
            linearLayout = itemView.findViewById(R.id.item_linear_layout);
        }
    }
}
