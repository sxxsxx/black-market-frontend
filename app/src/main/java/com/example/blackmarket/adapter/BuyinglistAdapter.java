package com.example.blackmarket.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.example.blackmarket.R;
import com.example.blackmarket.entities.BuyingShopItem;

import java.util.List;

public class BuyinglistAdapter extends RecyclerView.Adapter<BuyinglistAdapter.ViewHolder> {
    private List<BuyingShopItem.ShopData> dataList;
    private Context context;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    //定义一个设置监听的方法，便于主类设置点击后的动作
    public void setOnItemClickListener(BuyinglistAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public BuyinglistAdapter(List<BuyingShopItem.ShopData> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        BuyinglistAdapter.ViewHolder viewHolder;
        view = LayoutInflater.from(context).inflate(R.layout.item_buying_shoplist, null);
        viewHolder = new BuyinglistAdapter.ViewHolder(view);
        return viewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull BuyinglistAdapter.ViewHolder viewHolder, final int position) {
        String buyerImage = dataList.get(position).getPictureAddress();
        Glide.with(context).load(buyerImage).apply(RequestOptions.bitmapTransform(new CircleCrop())).into(viewHolder.buyerImage);
        viewHolder.userName.setText(dataList.get(position).getNickname());
        viewHolder.description.setText(dataList.get(position).getShoppingGuideDescription());//这里后面要加上字数限制
        viewHolder.shopPrice.setText(dataList.get(position).getShoppingGuidePrice()+"");
        viewHolder.areaName.setText(dataList.get(position).getCampus());
        //为layout添加一个OnClickListener，点击layout时就会调用onClick方法
        if (onItemClickListener!=null){
            viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //点击后调用主类设置的onItemClickListener，执行主类在其中设置的点击后的动作
                    onItemClickListener.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView buyerImage;
        private TextView userName;
        private TextView description;
        private TextView shopPrice;
        private TextView areaName;
        private RelativeLayout linearLayout;//item的layout

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            buyerImage = itemView.findViewById(R.id.user_img_buying);
            userName = itemView.findViewById(R.id.user_name);
            description = itemView.findViewById(R.id.buy_description);
            shopPrice = itemView.findViewById(R.id.buy_shop_price);
            areaName = itemView.findViewById(R.id.area_name_buy);
            linearLayout = itemView.findViewById(R.id.buy_item_lin_layout);
        }
    }
}
