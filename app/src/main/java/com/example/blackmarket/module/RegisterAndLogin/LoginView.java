package com.example.blackmarket.module.RegisterAndLogin;

public interface LoginView {

    void showSuccess(String token);

    void showFailure(Object response);
}
