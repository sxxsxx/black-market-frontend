package com.example.blackmarket.entities;

public class GetInformationSuccessRespond {
    /**
     * {
     *   "address": "string",
     *   "balance": 0,
     *   "campus": "string",
     *   "email": "string",
     *   "gender": "string",
     *   "nickname": "string",
     *   "pictureAddress": "string",
     *   "studentNumber": "string"
     * }
     */
    private String address;
    private double balance;
    private String campus;
    private String email;
    private String gender;
    private String nickname;
    private String pictureAddress;
    private String studentNumber;

    //快捷键alt+insert
    public GetInformationSuccessRespond(String address, double balance, String campus, String email, String gender, String nickname, String pictureAddress, String studentNumber) {
        this.address = address;
        this.balance = balance;
        this.campus = campus;
        this.email = email;
        this.gender = gender;
        this.nickname = nickname;
        this.pictureAddress = pictureAddress;
        this.studentNumber = studentNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPictureAddress() {
        return pictureAddress;
    }

    public void setPictureAddress(String pictureAddress) {
        this.pictureAddress = pictureAddress;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }
}
