package com.example.blackmarket.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.blackmarket.R;
import com.example.blackmarket.entities.OrderList;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    private List<OrderList.Order> dataList;
    private Context context;
    private OrderAdapter.OnItemClickListener onItemClickListener;

    public OrderAdapter(List<OrderList.Order> dataList, Context context){
        this.dataList = dataList;
        this.context = context;
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    //定义一个设置监听的方法，便于主类设置点击后的动作
    public void setOnItemClickListener(OrderAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        OrderAdapter.ViewHolder viewHolder;
        view = LayoutInflater.from(context).inflate(R.layout.order_item, null);
        viewHolder = new OrderAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderList.Order item = dataList.get(position);
        int state = item.getState();
        holder.price.setText(item.getProductPrice()+"黑币");
        holder.buyTime.setText(item.getBuyTime());
        holder.state.setText(state == 0 ? "交易中" : "交易成功");
        Glide.with(context).load(item.getProductPictureLocation()).into(holder.productImage);
        holder.productName.setText(item.getProductName());
        holder.confirm.setText(state == 0 ? "确认收货" : "评价");
        if(onItemClickListener != null){
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(position);
//                    int position = holder.getAdapterPosition();
//                    OrderList.Order order = dataList.get(position);
//                    Toast.makeText(context,"you clicked button" + position,Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linearLayout;//item的layout
        private TextView buyTime;
        private TextView state;
        private ImageView productImage;
        private TextView productName;
        private TextView price;
        private Button confirm;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            buyTime = itemView.findViewById(R.id.buy_time);
            state = itemView.findViewById(R.id.order_state);
            productImage = itemView.findViewById(R.id.item_img);
            productName = itemView.findViewById(R.id.item_name);
            price = itemView.findViewById(R.id.price);
            confirm = itemView.findViewById(R.id.confirm_order);
            linearLayout = itemView.findViewById(R.id.order_item_layout);
        }
    }
}
