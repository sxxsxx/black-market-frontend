package com.example.blackmarket.module.Me.Order;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.blackmarket.R;
import com.example.blackmarket.adapter.OrderAdapter;
import com.example.blackmarket.entities.OrderList;
import com.example.blackmarket.entities.SpacesItemDecoration;
import com.example.blackmarket.module.Base.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderActivity extends BaseActivity implements OrderView {
    private TextView noOrder;
    private ImageView btnBack;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private OrderAdapter adapter;
    private List<OrderList.Order> dataList = new ArrayList<>();
    private Context context;
    private OrderPresenter presenter;

    private int curPosition;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();
    }

    @Override
    protected void onBindView() {
        noOrder = findViewById(R.id.none_order);
        btnBack = findViewById(R.id.back_to_me);
        presenter = new OrderPresenter(this);
        context = this;
        recyclerView = findViewById(R.id.order_list);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        //设置recyclerview的item边距
        HashMap<String, Integer> spacesValue = new HashMap<>();
        spacesValue.put(SpacesItemDecoration.TOP_SPACE, 10);
        spacesValue.put(SpacesItemDecoration.BOTTOM_SPACE, 20);
        spacesValue.put(SpacesItemDecoration.LEFT_SPACE, 0);
        spacesValue.put(SpacesItemDecoration.RIGHT_SPACE, 0);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacesValue));
        adapter = new OrderAdapter(dataList,this);
        adapter.setOnItemClickListener(new OrderAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                curPosition = position;
//                Toast.makeText(context, "Click "+ position,Toast.LENGTH_SHORT).show();
                OrderList.Order item = dataList.get(position);
                if(item.getState() == 0){
                    //确认收货
//                    Log.e("productId",item.getProductId()+"");
                    AlertDialog.Builder dialog = new AlertDialog.Builder(OrderActivity.this);
                    dialog.setTitle("确认收货?");
                    dialog.setMessage("操作一旦完成，无法撤销");
                    dialog.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            presenter.confirm(item.getProductId());
                        }
                    });
                    dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    dialog.show();
                }else{
                    //评价
                    Toast.makeText(context,"后台正在加班开发中...",Toast.LENGTH_SHORT).show();
                }
            }
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void setButtonClick() {
        btnBack.setOnClickListener(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_order;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_to_me:{
                finish();
                break;
            }
            default:{
                break;
            }
        }
    }

    public void getData(){
        presenter.getData();
    }

    @Override
    public void showData(Object data) {
        List<OrderList.Order> list = (List<OrderList.Order>) data;
        if(list.size() == 0){
            noOrder.setVisibility(View.VISIBLE);
            return;
        }
        dataList.addAll(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showFailure(String msg) {
        Toast.makeText(this, msg,Toast.LENGTH_SHORT).show();
        Log.e("error",msg);
    }

    @Override
    public void confirmSuccess() {
        dataList.get(curPosition).setState(1);
        adapter.notifyDataSetChanged();
        Toast.makeText(this,"收货成功，订单已完成",Toast.LENGTH_SHORT).show();
    }
}
