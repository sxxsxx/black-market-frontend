package com.example.blackmarket.module.ErrandTask.DoRunning;

import android.os.Handler;
import android.util.Log;

import com.example.blackmarket.callback.IShopCallback;
import com.example.blackmarket.entities.DoRunningQuery;
import com.example.blackmarket.model.BlackMarketApi;
import com.example.blackmarket.module.Base.BasePresenter;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * @ProjectName: blackmarket$
 * @Package: com.example.blackmarket.module.ErrandTask.DoRunning$
 * @ClassName: DoRunningPresenter$
 * @Description: java类作用描述
 * @Author: yh
 * @CreateDate: 2020/11/24$ 17:21$
 * @UpdateUser: 更新者：
 * @UpdateDate: 2020/11/24$ 17:21$
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */

public class DoRunningPresenter extends BasePresenter<DoRunningView> implements IShopCallback {
    private Handler mHandler=new Handler();

    public DoRunningPresenter(DoRunningView view){
        attachView(view);

    }
    public void queryErrandTaskByPage(){
        BlackMarketApi.queryErrandTaskByPage(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        onFail(e.toString());
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (response.isSuccessful()){
                            Log.e("success","##");
                            assert response.body() != null;
                            String data = response.body().string();
                            mHandler.post(() -> onSuccess(data));

                        }
                        else {
                            Log.e("fail","##");
                            assert response.body() != null;
                            String msg = "code: "+ response.code() + " body: " +response.body().string();
                            Log.e("response failure",msg);
                            onFail(msg);
                        }
                    }
                }  );
    }


    @Override
    public void onSuccess(Object data) {
        Log.d("查询代跑腿任务:",data.toString());
        DoRunningQuery doRunningQuery = new Gson().fromJson((String) data,DoRunningQuery.class);
        this.getView().queryErrandTaskByPageSuccess(doRunningQuery);

    }

    @Override
    public void onFail(String msg) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onComplete() {

    }
}
