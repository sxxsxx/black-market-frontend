package com.example.blackmarket.module.SecondHandTrade.PostingSellOrWant;

public interface PostingSellOrWantView {
    //发布信息
    void postData(long posterId,double price,String place,String title,String description,String imageUrl);

    //发布成功
    void postSuccess(Object data);

    //发布失败（输入信息不完整）
    void postFailure(String msg);

    //获取学生ID成功回调
    void getStudentIdSuccess(Object data);
}
