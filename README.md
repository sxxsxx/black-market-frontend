# 华工线上黑市app前端

#### 介绍


#### 软件架构
mvp分层架构


#### 安装教程

无限点击下一步

#### 使用说明
***
调用顺序view<->presenter<->model，不允许跨层调用
如果页面不涉及网络请求/获取数据库数据（即没有业务逻辑）则不需要使用架构简单一个activity什么的就好

项目结构：
blackmarket
            ├─adapter 列表以及viewpager适配器
            ├─callback 定义数据请求成功后presenter的行为
            ├─entities 数据实体
            ├─model
            └─module
                ├─Base 基类
                ├─Home 主页
                └─SecondHandTrade 二手交易模块
                    ├─ItemDetail
                        │      ItemDetailActivity.java view层，需要实现view接口并且持有一个p层
                        │      ItemDetailPresenter.java p层，需要持有一个v层
                        │      ItemDetailView.java v层接口定义有哪些业务行为
                    ├─PostingSellOrWant
                    └─Shoplist
***

