package com.example.blackmarket.module.ErrandTask.DoRunning;

import com.example.blackmarket.entities.DoRunningItem;

import java.util.List;

/**
 * @ProjectName: blackmarket$
 * @Package: com.example.blackmarket.module.ErrandTask.DoRunning$
 * @ClassName: DoRunningView$
 * @Description: java类作用描述
 * @Author: 作者名
 * @CreateDate: 2020/11/24$ 20:09$
 * @UpdateUser: 更新者：
 * @UpdateDate: 2020/11/24$ 20:09$
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */

public interface DoRunningView {
//    public static List<DoRunningItem> m_list;
//    private static DoRunningView m_instance = new DoRunningView();
//    private  DoRunningView(){}
//    public static DoRunningView getInstance(){
//       return m_instance;
//    }
    void queryErrandTaskByPageSuccess(Object data);



}
