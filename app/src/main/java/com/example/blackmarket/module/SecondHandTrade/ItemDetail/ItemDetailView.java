package com.example.blackmarket.module.SecondHandTrade.ItemDetail;

public interface ItemDetailView {
    void setData(String seller,String buyer,int item_id, int type);

    void showSuccess();

    void showFailure(String message);
}
