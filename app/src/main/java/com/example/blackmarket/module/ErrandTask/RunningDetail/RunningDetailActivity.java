package com.example.blackmarket.module.ErrandTask.RunningDetail;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.example.blackmarket.R;
import com.example.blackmarket.entities.DoRunningAcceptTaskReply;
import com.example.blackmarket.module.Base.BaseActivity;
import com.example.blackmarket.module.Base.BaseApplication;


public class RunningDetailActivity extends BaseActivity implements RunningDetailView {
    private TextView usernameText;
    private TextView campusText;
    private TextView priceText;
    private TextView beginText;
    private TextView destinationText;
    private TextView timeText;
    private TextView weightText;
    private TextView noteText;
    private ImageView userImage;
    private ImageView detailBack;
    private Button accept_errand_task;

    private RunningDetailPresenter runningDetailPresenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        runningDetailPresenter = new RunningDetailPresenter(this);
        showData();
    }

    @Override
    protected void onBindView() {
        usernameText = findViewById(R.id.username);
        campusText = findViewById(R.id.campus);
        priceText = findViewById(R.id.price);
        beginText = findViewById(R.id.begin);
        destinationText = findViewById(R.id.destination);
        timeText = findViewById(R.id.time);
        weightText = findViewById(R.id.weight);
        noteText = findViewById(R.id.description);
        userImage = findViewById(R.id.user_img);
        detailBack = findViewById(R.id.detail_back);
        accept_errand_task = findViewById(R.id.accept_errand_task);
    }

    @Override
    protected void setButtonClick() {
        usernameText.setOnClickListener(this);
        campusText.setOnClickListener(this);
        priceText.setOnClickListener(this);
        beginText.setOnClickListener(this);
        destinationText.setOnClickListener(this);;
        timeText.setOnClickListener(this);
        weightText.setOnClickListener(this);
        noteText.setOnClickListener(this);
        userImage.setOnClickListener(this);
        detailBack.setOnClickListener(this);
        accept_errand_task.setOnClickListener(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_running_detail;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.accept_errand_task:{
                Log.e("你按了接单",this.getIntent().getStringExtra("errandID")
                        +BaseApplication.get("studentNumber", "1"));

                runningDetailPresenter.acceptErrandTask(this.getIntent().getStringExtra("errandID")
                        , BaseApplication.get("studentNumber", "1"));
            }
            case R.id.detail_back:{
                finish();
                break;
            }
            default:{
               break;
            }
        }
    }

    public void showData(){
        Glide.with(this).load(this.getIntent().getStringExtra("user_image")).into(userImage);
        usernameText.setText(this.getIntent().getStringExtra("nickname"));
        campusText.setText(this.getIntent().getStringExtra("campus"));
        priceText.setText(this.getIntent().getStringExtra("price"));
        beginText.setText(this.getIntent().getStringExtra("startPlace"));
        destinationText.setText(this.getIntent().getStringExtra("endPlace"));
        weightText.setText(this.getIntent().getStringExtra("weight"));
        timeText.setText(this.getIntent().getStringExtra("endTime"));
        noteText.setText(this.getIntent().getStringExtra("note"));
    }

    @Override
    public void acceptErrandTask(Object data) {
        DoRunningAcceptTaskReply doRunningAcceptTaskReply = (DoRunningAcceptTaskReply)data;
        if (doRunningAcceptTaskReply.success==true){
            Toast.makeText(this,"你成功接收了此跑腿 ",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this,"啊哦,可能出了点问题,你没有接收"+doRunningAcceptTaskReply.message,Toast.LENGTH_SHORT).show();
        }
    }
}
