package com.example.blackmarket.entities;

import android.util.Log;
import android.widget.EditText;

import com.example.blackmarket.module.Base.BaseActivity;
import com.example.blackmarket.module.Base.BaseApplication;

/**
 * @ProjectName: blackmarket$
 * @Package: com.example.blackmarket.entities$
 * @ClassName: DoRunningItem$
 * @Description: java类作用描述
 * @Author: yh
 * @CreateDate: 2020/11/14$ 10:55$
 * @UpdateUser: 更新者：
 * @UpdateDate: 2020/11/14$ 10:55$
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */

public class DoRunningItem {
    private String price;
    private String startPlace;
    private String endPlace;
    private String weight;
    private String note;
    private String errandId;
    private String endTime;
    private String nickname;
    private String pictureAddress;
    private String campus;

    public DoRunningItem(){

    }

    public String getProposerNumber() {
        return proposerNumber;
    }

    public void setProposerNumber(String proposerNumber) {
        this.proposerNumber = proposerNumber;
    }

    private String proposerNumber;

    public String getErrandId() {
        return errandId;
    }

    public void setErrandId(String errandId) {
        this.errandId = errandId;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPictureAddress() {
        return pictureAddress;
    }

    public void setPictureAddress(String pictureAddress) {
        this.pictureAddress = pictureAddress;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }


    public DoRunningItem(String endPlace, String note, String price,
                         String proposerNumber, String startPlace, String weight){
            this.endPlace = endPlace;
            this.weight = weight;
            this.price = price;
            this.startPlace = startPlace;
            this.note = note;
            this.proposerNumber = BaseApplication.get("studentNumber","#");
            if(this.proposerNumber=="#"){
                Log.e("DoRunningItem.java line35","饿");
            }


    }

    public String getstartPlace() {
        return startPlace;
    }

    public void setstartPlace(String startPlace) {
        this.startPlace = startPlace;
    }

    public String getendPlace() {
        return endPlace;
    }

    public void setendPlace(String endPlace) {
        this.endPlace = endPlace;
    }

    public String getweight() {
        return weight;
    }

    public void setweight(String weight) {
        this.weight = weight;
    }

    public String getnote() {
        return note;
    }

    public void setnote(String note) {
        this.note = note;
    }

    public String getprice() {
        return price;
    }

    public void setprice(String price) {
        this.price = price;
    }
}
