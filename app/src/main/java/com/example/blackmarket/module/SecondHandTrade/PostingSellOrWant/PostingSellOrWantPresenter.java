package com.example.blackmarket.module.SecondHandTrade.PostingSellOrWant;

import android.os.Handler;
import android.util.Log;

import com.example.blackmarket.callback.IShopCallback;
import com.example.blackmarket.entities.SuccessResponse;
import com.example.blackmarket.model.BlackMarketApi;
import com.example.blackmarket.module.Base.BasePresenter;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class PostingSellOrWantPresenter extends BasePresenter<PostingSellOrWantView> implements IShopCallback {
    private Handler mHandler;
    //构造函数
    public PostingSellOrWantPresenter(PostingSellOrWantView view) {
        attachView(view);
        mHandler=new Handler();
    }

    public void postingData(String type,String campus,String shoppingGuideBuyerId,String shoppingGuideDescription,String shoppingGuideName,String shoppingGuidePictureLocation,double shoppingGuidePrice){
        if(type.equals("求购")){
            BlackMarketApi.postingData(campus,shoppingGuideBuyerId,shoppingGuideDescription,shoppingGuideName,shoppingGuidePictureLocation,shoppingGuidePrice,
                    new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            onFail(e.toString());
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if (response.isSuccessful()){
                                assert response.body() != null;
                                String data = response.body().string();
                                mHandler.post(() -> onSuccess(data));
                            }
                            else {
                                assert response.body() != null;
                                String msg = "code: "+ response.code() + " body: " +response.body().string();
                                Log.e("response failure",msg);
                                mHandler.post(()->onFail(msg));
                            }
                        }
                    });
        }else{
            BlackMarketApi.postingData1(campus,shoppingGuideBuyerId,shoppingGuideDescription,shoppingGuideName,shoppingGuidePictureLocation,shoppingGuidePrice,new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    onFail(e.toString());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()){
                        assert response.body() != null;
                        String data = response.body().string();
                        mHandler.post(() -> onSuccess(data));
                    }
                    else {
                        assert response.body() != null;
                        String msg = "code: "+ response.code() + " body: " +response.body().string();
                        Log.e("response failure",msg);
                        mHandler.post(()->onFail(msg));
                    }
                }
            });
        }

    }

    public void getStudentId(){
        BlackMarketApi.getStudentId(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    String data = response.body().string();
                    mHandler.post(() -> onSuccess(data));
                }
                else {
                    assert response.body() != null;
                    String msg = "code: "+ response.code() + " body: " +response.body().string();
                    Log.e("response failure",msg);
                    mHandler.post(()->onFail(msg));
                }
            }
        });
    }

    @Override
    public void onSuccess(Object data) {
//        //获取用户id的回调
//        GetInformationSuccessRespond getInformationSuccessRespond=new Gson().fromJson(((String)data),GetInformationSuccessRespond.class);
//        this.getView().getStudentIdSuccess(getInformationSuccessRespond);

        SuccessResponse successResponse = new Gson().fromJson(((String)data), SuccessResponse.class);
        this.getView().postSuccess(successResponse);
    }

    @Override
    public void onFail(String msg) {
        if(isViewAttached()){
            this.getView().postFailure(msg);
        }
    }

    @Override
    public void onError() {

    }

    @Override
    public void onComplete() {

    }
}
