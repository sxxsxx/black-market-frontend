package com.example.blackmarket.module.Me;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.blackmarket.R;
import com.example.blackmarket.module.Base.BaseActivity;

public class ChangeInfoActivity extends BaseActivity implements ChangeInfo {
    private EditText nicknameText;
    private EditText addressText;
    private Button submit;
    private ImageView back;
    private Spinner campusOption;

    private String studentNumber;
    private String address;
    private String nickname;
    private String campus;

    private String campusAfter;
    private String nicknameAfter;
    private String addressAfter;

    private ChangeInfoPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ChangeInfoPresenter(this);
        Intent intent = getIntent();
        studentNumber = intent.getStringExtra("studentNumber");
        address = intent.getStringExtra("address");
        nickname = intent.getStringExtra("nickname");
        campus = intent.getStringExtra("campus");
        nicknameText.setText(nickname);
        //nicknameText.setHint(nickname);
        addressText.setText(address);
        //addressText.setHint(address);
        campusOption.setSelection(campus.equals("南校")? 0 : 1);
    }

    @Override
    protected void onBindView() {
        nicknameText = findViewById(R.id.edit_nickname);
        addressText = findViewById(R.id.edit_address);
        campusOption = findViewById(R.id.edit_campus);
        submit = findViewById(R.id.submit_change);
        back = findViewById(R.id.back_to_me);
    }

    @Override
    protected void setButtonClick() {
        submit.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.change_personal_info;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit_change:
                submitInfo();
                break;
            case R.id.back_to_me:
                finish();
                break;
            default:
                break;
        }
    }

    public void submitInfo(){
        nicknameAfter = nicknameText.getText().toString();
        addressAfter = addressText.getText().toString();
        campusAfter = campusOption.getSelectedItem().toString();
        if(nicknameAfter.equals(this.nickname) && addressAfter.equals(this.address) && campusAfter.equals(campus)){
            showToast("你没有做任何修改噢！");return;
        }else if(nicknameAfter.equals("") && addressAfter.equals("")){
            showToast("昵称和地址都不能为空噢！");return;
        }else if(nicknameAfter.equals("")){
            showToast("昵称不能为空噢！");return;
        }else if(addressAfter.equals("")){
            showToast("地址不能为空噢！");return;
        }else{
            presenter.submitChange(studentNumber, nicknameAfter, addressAfter, campusAfter);
            return;
        }
    }

    public void showToast(String msg){
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
//        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }

    @Override
    public void showSuccess() {
        showToast("修改成功！即将自动返回上一页面");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.putExtra("address",addressAfter);
                intent.putExtra("nickname",nicknameAfter);
                intent.putExtra("campus",campusAfter);
                setResult(RESULT_OK, intent);
                finish();
            }
        },Toast.LENGTH_SHORT);
    }

    @Override
    public void showFailure(String msg) {
        showToast("修改失败：" + msg);
    }
}
