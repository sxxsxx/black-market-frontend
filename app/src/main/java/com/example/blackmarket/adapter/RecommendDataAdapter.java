package com.example.blackmarket.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.recyclerview.widget.RecyclerView;
import com.example.blackmarket.R;
import com.example.blackmarket.entities.RecommendData;
import java.util.List;

public class RecommendDataAdapter extends RecyclerView.Adapter<RecommendDataAdapter.ViewHolder>{
    private List<RecommendData> mRecommendDataList;

    static class ViewHolder extends RecyclerView.ViewHolder{
        View RecommendDataView;
        ImageView RecommendDataImage;
        TextView RecommendDataName;

        //构造函数
        //要传入View参数，通常就是RecyclerView子项的最外层布局
        public ViewHolder(View view){
            super(view);
            RecommendDataView=view;
            RecommendDataImage=(ImageView)view.findViewById(R.id.recommend_data_image);
            RecommendDataName=(TextView)view.findViewById(R.id.recommend_data_name);
        }
    }

    public RecommendDataAdapter(List<RecommendData> recommenddataList){
        mRecommendDataList=recommenddataList;
    }

    //用于创建ViewHolder实例
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recommend_data_item,parent,false);
        final ViewHolder holder=new ViewHolder(view);

        //首页分类图标的点击事件
        holder.RecommendDataView.setOnClickListener((new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int position=holder.getAdapterPosition();
                RecommendData recommenddata=mRecommendDataList.get(position);
                Toast.makeText(v.getContext(),"you clicked on "+recommenddata.getName(),Toast.LENGTH_SHORT).show();
            }
        }));
        return holder;
    }

    //对RecyclerView子项数据进行赋值，会在每个子项被滚动到屏幕内时被执行
    //通过position得到当前项的Fruit实例，然后再将数据设置到ViewHolder的ImageView和TextView中
    @Override
    public void onBindViewHolder(ViewHolder holder,int position){
        RecommendData recommenddata=mRecommendDataList.get(position);
        holder.RecommendDataImage.setImageResource(recommenddata.getImageId());
        holder.RecommendDataName.setText(recommenddata.getName());
    }

    @Override
    public int getItemCount(){
        return mRecommendDataList.size();
    }
}