package com.example.blackmarket;

import com.example.blackmarket.module.Base.BaseApplication;
import com.example.blackmarket.utils.OkHttpClientUtil;

public class BlackMarketApplication extends BaseApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    /**
     * 在这里进行一些网络初始化，版本检查等等
     */
    private void init(){
        OkHttpClientUtil.getInstance();
    }
}
