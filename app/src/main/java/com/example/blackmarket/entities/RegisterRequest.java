package com.example.blackmarket.entities;

public class RegisterRequest {
    private String mail;
    private String password;
    private String studentNumber;
    private String validateCode;

    public RegisterRequest(
            String studentNumber,
            String password,
            String mail,
            String validateCode
    ){
        this.studentNumber = studentNumber;
        this.password = password;
        this.mail = mail;
        this.validateCode = validateCode;
    }

    // Getter Methods

    public String getMail() {
        return mail;
    }

    public String getPassword() {
        return password;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public String getValidateCode() {
        return validateCode;
    }

    // Setter Methods

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public void setValidateCode(String validateCode) {
        this.validateCode = validateCode;
    }
}