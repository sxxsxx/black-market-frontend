package com.example.blackmarket.entities;

public class User {
    private String pictureAddress;
    private String nickname;
    private String email;
    private String studentNumber;
    private String campus;
    private String address;
    private double balance;


    public String getAddress() {
        return address;
    }

    public double getBalance() {
        return balance;
    }

    public String getCampus() {
        return campus;
    }

    public String getNickname() {
        return nickname;
    }

    public String getEmail() {
        return email;
    }

    public String getPictureAddress() {
        return pictureAddress;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setPictureAddress(String pictureAddress) {
        this.pictureAddress = pictureAddress;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }
}
