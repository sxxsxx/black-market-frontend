package com.example.blackmarket.module.RegisterAndLogin;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.blackmarket.R;
import com.example.blackmarket.entities.SuccessResponse;
import com.example.blackmarket.module.Base.BaseActivity;

import java.util.Timer;
import java.util.TimerTask;

public class RegisterActivity extends BaseActivity implements RegisterView {

    private ImageView imageView;
    private EditText studentNumber;
    private EditText password;
    private EditText confirmPassword;
    private EditText email;
    private EditText verifyCode;
    private Button getVerifyCode;
    private Button register;
    private RegisterPresenter registerPresenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init();
        registerPresenter = new RegisterPresenter(this);
    }

    @Override
    protected void onBindView() {
        studentNumber = findViewById(R.id.student_number);
        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirmPassword);
        email = findViewById(R.id.email);
        verifyCode = findViewById(R.id.verifyCode);
        getVerifyCode = findViewById(R.id.getVerifyCode);
        register = findViewById(R.id.register);
        imageView = findViewById(R.id.imageView);
    }

    @Override
    protected void setButtonClick() {
        getVerifyCode.setOnClickListener(this);
        register.setOnClickListener(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.register;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.getVerifyCode:
                System.out.println(email.getText().toString());
                String emailText = email.getText().toString();
                if(emailText.contains("@mail.scut.edu.cn")){
                    registerPresenter.getVerifyCode(emailText);
                    Toast.makeText(this, "验证码已发送，请注意查收。\n 60s后可重新获取验证码。", Toast.LENGTH_SHORT).show();
                    getVerifyCode.setEnabled(false);
                    getVerifyCode.setText("60s后重试");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getVerifyCode.setEnabled(true);
                            getVerifyCode.setText("获取验证码");
                        }
                    },1000 * 60);
                }else{
                    Toast.makeText(this, "请使用后缀为‘@mail.scut.edu.cn’的华工邮箱注册", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.register:
                System.out.println(confirmPassword.getText().toString());
                registerPresenter.register(studentNumber.getText().toString(), password.getText().toString(), confirmPassword.getText().toString(), email.getText().toString(), verifyCode.getText().toString());
                break;
        }
    }


    //viewFunction
    @Override
    public void clickVerify() {

    }

    @Override
    public void changeText() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setUsernameError() {

    }

    @Override
    public void setPasswordError() {

    }

    @Override
    public void showFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSuccess(Object data) {
        SuccessResponse successResponse = (SuccessResponse)data;
        boolean success = successResponse.getSuccess();
        if (success){
            Toast.makeText(this, successResponse.getMessage(),Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
        }
        else
            Toast.makeText(this, successResponse.getMessage(),Toast.LENGTH_SHORT).show();
    }
}
