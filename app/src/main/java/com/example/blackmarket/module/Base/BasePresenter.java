package com.example.blackmarket.module.Base;

/**
 * 作为BasePresenter的过渡类
 * model层迭代到api形式，所有model函数都在一个类里，presenter不需要再与model绑定
 * @param <V>
 */
public class BasePresenter<V> {
    private V view;
    /**
     * 绑定view，一般在初始化中调用该方法
     */
    public void attachView(V view) {
        this.view = view;
    }
    /**
     * 断开view，一般在onDestroy中调用
     */
    public void detachView() {
        this.view = null;
    }
    /**
     * 是否与View建立连接
     * 每次调用业务请求的时候都要出先调用方法检查是否与View建立连接
     */
    public boolean isViewAttached(){
        return view != null;
    }
    /**
     * 获取连接的view
     */
    public V getView(){
        return view;
    }
}
