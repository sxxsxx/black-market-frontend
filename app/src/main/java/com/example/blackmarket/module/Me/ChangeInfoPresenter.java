package com.example.blackmarket.module.Me;

import android.os.Handler;
import android.util.Log;

import com.example.blackmarket.callback.IShopCallback;
import com.example.blackmarket.entities.SuccessResponse;
import com.example.blackmarket.model.BlackMarketApi;
import com.example.blackmarket.module.Base.BasePresenter;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class ChangeInfoPresenter extends BasePresenter<ChangeInfo> implements IShopCallback {
    private Handler mHandler;
    public ChangeInfoPresenter(ChangeInfo view){
        attachView(view);
        mHandler=new Handler();
    }

    public void submitChange(String studentNumber, String nickname, String address, String campus){
        BlackMarketApi.changePersonalInfo(studentNumber, nickname, address, campus, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    String data = response.body().string();
                    Log.e("return",data);
                    mHandler.post(() -> onSuccess(data));
                }
                else {
                    assert response.body() != null;
                    String msg = "code: "+ response.code() + " body: " +response.body().string();
                    Log.e("response failure",msg);
                    mHandler.post(()->onFail(msg));
                }
            }
        });
    }

    @Override
    public void onSuccess(Object data) {
//        Log.e("submit success",data.toString());
        SuccessResponse response = new Gson().fromJson((String) data, SuccessResponse.class);
        if(response.getSuccess()){
            if(isViewAttached()) {
                this.getView().showSuccess();
            }
        }else{
            if(isViewAttached()) {
                this.getView().showFailure(response.getMessage());
            }
        }

    }

    @Override
    public void onFail(String msg) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onComplete() {

    }
}
