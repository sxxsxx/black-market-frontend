package com.example.blackmarket.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import java.util.List;

public class LoopPagerAdapter extends PagerAdapter {
    private List<Integer> sPics=null;

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        //防止数组越界，取模
        int realPosition=position%sPics.size();
        ImageView imageView=new ImageView(container.getContext());
        imageView.setImageResource(sPics.get(realPosition));
        //添加到容器里
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        if(sPics!=null){
            //可以轮播无数次——无限轮播
            return Integer.MAX_VALUE;
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }

    public void setData(List<Integer> sColors) {
        this.sPics=sColors;
    }

    public int getDataRealSize(){
        if(sPics!=null){
            return sPics.size();
        }
        return 0;
    }
}
