package com.example.blackmarket.module.RegisterAndLogin;


public interface RegisterView {

    void changeText();

    void showProgress();

    void hideProgress();

    void setUsernameError();

    void setPasswordError();

    void showFailure(String msg);

    void showSuccess(Object data);

    void clickVerify();

}