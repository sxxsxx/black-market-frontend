package com.example.blackmarket.module.QuestionAnswer.Posting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.blackmarket.R;
import com.example.blackmarket.entities.SuccessResponse;
import com.example.blackmarket.module.Base.BaseActivity;
import com.example.blackmarket.module.SecondHandTrade.PostingSuccess.PostingSuccessActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuestionAnswerPostingActivity extends BaseActivity implements QuestionAnswerPostingView{
    private ImageButton question_posting_back;
    private ImageButton question_posting_confirm;
    private TextView question_posting_title;
    private TextView question_posting_price;
    private EditText question_posting_description;
    private QuestionAnswerPostingPresenter questionAnswerPostingPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        questionAnswerPostingPresenter=new QuestionAnswerPostingPresenter(this);
    }

    @Override
    protected void onBindView() {
        question_posting_back=findViewById(R.id.question_posting_back);
        question_posting_confirm=findViewById(R.id.question_posting_confirm);
        question_posting_description=findViewById(R.id.question_posting_description);
        question_posting_price=findViewById(R.id.question_posting_price);
        question_posting_title=findViewById(R.id.question_posting_title);
    }

    @Override
    protected void setButtonClick() {
        question_posting_back.setOnClickListener(this);
        question_posting_confirm.setOnClickListener(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.question_answer_posting;
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.question_posting_back:{
                finish();
                break;
            }
            case R.id.question_posting_confirm:{
                //输入完整的话
                if(isComplete()){
                    questionAnswerPostingPresenter.question_posting(question_posting_description.getText().toString(),Double.parseDouble(question_posting_price.getText().toString()),question_posting_title.getText().toString());
                }
                break;
            }
            default:break;
        }
    }

    //判断输入是否完整
    private boolean isComplete() {
        //判断是否输入标题
        if (question_posting_title.getText().toString()==""||question_posting_title.getText().toString().equals("")){
            Toast.makeText(this,"请输入标题",Toast.LENGTH_SHORT).show();
            return false;
        }
        //判断是否输入价格
        if (question_posting_price.getText().toString()==""||question_posting_price.getText().toString().equals("")){
            Toast.makeText(this,"请输入价格",Toast.LENGTH_SHORT).show();
            return false;
        }
        //判断价格是否合法
        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(question_posting_price.getText().toString());
        if(!m.matches() ){
            Toast.makeText(this,"请输入合法数字", Toast.LENGTH_SHORT).show();
            return false;
        }
        //判断是否输入问题描述
        if (question_posting_description.getText().toString()==""||question_posting_description.getText().toString().equals("")){
            Toast.makeText(this,"请输入问题描述",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void postSuccess(Object data){
        SuccessResponse successResponse = (SuccessResponse)data;
        boolean success = successResponse.getSuccess();
        if (success){
            Toast.makeText(this, successResponse.getMessage(),Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, PostingSuccessActivity.class);
            startActivity(intent);
        }
        else
            Toast.makeText(this, successResponse.getMessage(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void postFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
