package com.example.blackmarket.module.LaunchPage;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.blackmarket.module.Home.MainActivity;
import com.example.blackmarket.module.RegisterAndLogin.LoginActivity;
import com.example.blackmarket.utils.JwtUtil;

import static com.example.blackmarket.module.Base.BaseApplication.context;
import static com.example.blackmarket.module.Base.BaseApplication.get;

/**
 * 启动页，不进行setContentView()减少加载视图的时间
 */
public class LaunchActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //将原来在application开启activity操作迁移到此处，避免开启的activity任务栈只能是singleTask的
        if (JwtUtil.validateToken(get("token",null))){
            Intent intent = new Intent(context(), MainActivity.class);
            startActivity(intent);
        }
        else{
            Intent intent = new Intent(context(), LoginActivity.class);
            startActivity(intent);
        }
    }
}
