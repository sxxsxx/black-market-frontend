package com.example.blackmarket.utils;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.nio.charset.StandardCharsets;
import java.util.Date;

public class JwtUtil {
    public static boolean validateToken(String token){
        if (token!=null && !token.equals("")){
            String[] arg = token.split("\\.");
            byte[] bytes= Base64.decode(arg[1],Base64.URL_SAFE);
            String s = new String(bytes, StandardCharsets.UTF_8);
            JsonObject jsonObject = new Gson().fromJson(s,JsonObject.class);
            long d = jsonObject.get("exp").getAsLong();
            long cur =new Date().getTime()/1000;
            return cur<d;
        }
        return false;
    }
}