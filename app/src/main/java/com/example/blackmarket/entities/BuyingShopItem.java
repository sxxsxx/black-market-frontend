package com.example.blackmarket.entities;

import java.util.List;

/**
 * {
 *   "current": 0,
 *   "pages": 0,
 *   "records": [
 *     {
 *       "campus": "南校",
 *       "nickname": "阿聪",
 *       "pictureAddress": "static/personalPicture",
 *       "productId": 100010,
 *       "shoppingGuideDescription": "要一个很大很大的书包",
 *       "shoppingGuideName": "书包",
 *       "shoppingGuidePictureLocation": "http://127.1.0.1:111111111111111111111111111111111.jpg",
 *       "shoppingGuidePrice": 20
 *     }
 *   ],
 *   "searchCount": true,
 *   "size": 0,
 *   "total": 0
 * }
 */
public class BuyingShopItem {
    private List<ShopData> records;
    private int code;
    private int current;
    private boolean searchCount;
    private int size;
    private int total;

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public boolean isSearchCount() {
        return searchCount;
    }

    public void setSearchCount(boolean searchCount) {
        this.searchCount = searchCount;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ShopData> getRecords() {
        return records;
    }

    public int getCode() {
        return code;
    }

    public void setRecords(List<ShopData> records) {
        this.records = records;
    }

    public void setCode(int code) {
        this.code = code;
    }

    /**
     * "records": [
     *  *     {
     *  *       "campus": "南校",
     *  *       "nickname": "阿聪",
     *  *       "pictureAddress": "static/personalPicture",
     *  *       "productId": 100010,
     *  *       "shoppingGuideDescription": "要一个很大很大的书包",
     *  *       "shoppingGuideName": "书包",
     *  *       "shoppingGuidePictureLocation": "http://127.1.0.1:111111111111111111111111111111111.jpg",
     *  *       "shoppingGuidePrice": 20
     *  *     }
     *  *   ],
     */
    public static class ShopData{
        private String campus;
        private String nickname;
        private String pictureAddress;
        private int productId;
        private String shoppingGuideDescription;
        private String shoppingGuideName;
        private String shoppingGuidePictureLocation;
        private double shoppingGuidePrice;

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public String getShoppingGuideName() {
            return shoppingGuideName;
        }

        public void setShoppingGuideName(String shoppingGuideName) {
            this.shoppingGuideName = shoppingGuideName;
        }

        public String getShoppingGuidePictureLocation() {
            return shoppingGuidePictureLocation;
        }

        public void setShoppingGuidePictureLocation(String shoppingGuidePictureLocation) {
            this.shoppingGuidePictureLocation = shoppingGuidePictureLocation;
        }

        public String getCampus() {
            return campus;
        }

        public void setCampus(String campus) {
            this.campus = campus;
        }
        public void setPictureAddress(String pictureAddress) {
            this.pictureAddress = pictureAddress;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public void setShoppingGuideDescription(String shoppingGuideDescription) {
            this.shoppingGuideDescription = shoppingGuideDescription;
        }

        public void setShoppingGuidePrice(double shoppingGuidePrice) {
            this.shoppingGuidePrice = shoppingGuidePrice;
        }

        public String getPictureAddress() {
            return pictureAddress;
        }

        public String getNickname() {
            return nickname;
        }

        public String getShoppingGuideDescription() {
            return shoppingGuideDescription;
        }

        public double getShoppingGuidePrice() {
            return shoppingGuidePrice;
        }

    }
}
