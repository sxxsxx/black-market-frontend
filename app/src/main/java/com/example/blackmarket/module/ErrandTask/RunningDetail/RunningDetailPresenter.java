package com.example.blackmarket.module.ErrandTask.RunningDetail;

import android.os.Handler;
import android.util.Log;

import com.example.blackmarket.callback.IShopCallback;
import com.example.blackmarket.entities.DoRunningAcceptTaskReply;

import com.example.blackmarket.model.BlackMarketApi;
import com.example.blackmarket.module.Base.BasePresenter;


import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * @ProjectName: blackmarket$
 * @Package: com.example.blackmarket.module.ErrandTask.DoRunning$
 * @ClassName: DoRunningPresenter$
 * @Description: java类作用描述
 * @Author: yh
 * @CreateDate: 2020/11/24$ 17:21$
 * @UpdateUser: 更新者：
 * @UpdateDate: 2020/11/24$ 17:21$
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */

public class RunningDetailPresenter extends BasePresenter<RunningDetailView> implements IShopCallback {
    private Handler mHandler=new Handler();

    public RunningDetailPresenter(RunningDetailView view){
        attachView(view);

    }
    public void acceptErrandTask(String errandID,String acceptorID){
        BlackMarketApi.acceptErrandTask(errandID,acceptorID,new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    Log.e("success","##");
                    assert response.body() != null;
                    String data = response.body().string();
                    mHandler.post(() -> onSuccess(data));

                }
                else {
                    Log.e("fail","##");
                    assert response.body() != null;
                    String msg = "code: "+ response.code() + " body: " +response.body().string();
                    Log.e("response failure",msg);
                    onFail(msg);
                }
            }
        }  );
    }


    @Override
    public void onSuccess(Object data) {
        Log.d("接任务:",data.toString());
        DoRunningAcceptTaskReply doRunningAcceptTaskReply = new Gson().fromJson((String) data,DoRunningAcceptTaskReply.class);
        this.getView().acceptErrandTask(doRunningAcceptTaskReply);

    }

    @Override
    public void onFail(String msg) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onComplete() {

    }
}
