package com.example.blackmarket.module.Me;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.blackmarket.R;

public class MenuItemLayout extends LinearLayout {
    private Context mContext;
    private ImageView imageView;
    private TextView titleText;
    private TextView hintText;
    private String hint;
    private int iconId;
    private String title;

    public MenuItemLayout(Context context) {
        this(context, null);
    }

    public MenuItemLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MenuItemLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.menu_item_layout,this);
        init(context, attrs);
    }

    protected void init(Context context, AttributeSet attrs){
        mContext = context;
        onBindView();
        TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.MenuItemLayout);
        setIconId(a.getResourceId(R.styleable.MenuItemLayout_icon_reference,10000));
        setTitle(a.getString(R.styleable.MenuItemLayout_title_text));
        setHint(a.getString(R.styleable.MenuItemLayout_hint));
    }

    protected void onBindView(){
        imageView = findViewById(R.id.menu_item_icon);
        titleText = findViewById(R.id.menu_item_text);
        hintText = findViewById(R.id.menu_item_hint);
    }

    public int getIconId(){
        return iconId;
    }
    public String getTitle(){
        return title;
    }
    public String getHint(){
        return hint;
    }
    public void setIconId(int iconId){
        if (iconId != 10000) {
            this.iconId = iconId;
            imageView.setImageResource(iconId);
        }
    }

    public void setTitle(String title){
        if(title != null){
            this.title = title;
            titleText.setText(title);
        }
    }

    public void setHint(String hint){
        if(hint != null){
            this.hint = hint;
            hintText.setText(hint);
        }
    }


}
