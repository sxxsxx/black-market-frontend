package com.example.blackmarket.entities;

public class RecommendData {
    private String name;
    private int imageId;

    public RecommendData(String name,int imageId){
        this.name=name;
        this.imageId=imageId;
    }

    public String getName(){
        return name;
    }

    public int getImageId(){
        return imageId;
    }
}