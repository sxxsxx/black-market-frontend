package com.example.blackmarket.adapter;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.example.blackmarket.module.SecondHandTrade.Shoplist.BuyingFragment;
import com.example.blackmarket.module.SecondHandTrade.Shoplist.SellingFragment;

public class ShopListFragmentPagerAdapter extends FragmentPagerAdapter {
    private SellingFragment sellingFragment;
    private BuyingFragment buyingFragment;

    public ShopListFragmentPagerAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        sellingFragment = new SellingFragment();
        buyingFragment = new BuyingFragment();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return position==0?sellingFragment:buyingFragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
