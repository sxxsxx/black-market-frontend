package com.example.blackmarket.entities;

/**
 * @ProjectName: blackmarket$
 * @Package: com.example.blackmarket.entities$
 * @ClassName: DoRunningAcceptTaskReply$
 * @Description: java类作用描述
 * @Author: yh
 * @CreateDate: 2020/11/24$ 20:52$
 * @UpdateUser: 更新者：
 * @UpdateDate: 2020/11/24$ 20:52$
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */

public class DoRunningAcceptTaskReply {
    public DoRunningAcceptTaskReply(){

    }
    public String status;
    public boolean success;
    public String message;
}
