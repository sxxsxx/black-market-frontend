package com.example.blackmarket.module.SecondHandTrade.PostingSuccess;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.example.blackmarket.R;
import com.example.blackmarket.module.Base.BaseActivity;
import com.example.blackmarket.module.Home.MainActivity;

public class PostingSuccessActivity extends BaseActivity {

    private Button posting_success;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onBindView() {
        posting_success=(Button)findViewById(R.id.posting_success);
    }

    @Override
    protected void setButtonClick() {
        posting_success.setOnClickListener(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.posting_success;
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.posting_success:{
                Intent intent=new Intent(this, MainActivity.class);
                startActivity(intent);
            }
        }
    }
}
