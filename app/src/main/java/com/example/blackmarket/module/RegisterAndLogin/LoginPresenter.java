package com.example.blackmarket.module.RegisterAndLogin;

import android.os.Handler;
import android.util.Log;

import com.example.blackmarket.entities.FailureResponse;
import com.example.blackmarket.entities.JsonWebToken;
import com.example.blackmarket.model.BlackMarketApi;
import com.example.blackmarket.module.Base.BasePresenter;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class LoginPresenter extends BasePresenter<LoginView> {
    private Handler mHandler;
    public LoginPresenter(LoginView loginView){
        attachView(loginView);
        mHandler=new Handler();
    }

    public void login(String studentNumber, String password){
        BlackMarketApi.login(studentNumber, password, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    String data = response.body().string();
                    mHandler.post(() -> onSuccess(data));
                }
                else {
                    assert response.body() != null;
                    String msg = response.body().string();
                    Log.e("response failure",msg);
                    mHandler.post(()->onFail(msg));
                }
            }
        });
    }

    public void onSuccess(Object data) {
        Log.e("return token",data.toString());
        JsonWebToken jsonWebToken = new Gson().fromJson((String) data, JsonWebToken.class);
        String token = jsonWebToken.getToken();
        if(isViewAttached())
            this.getView().showSuccess(token);
    }


    public void onFail(String msg) {
        FailureResponse failureResponse = new Gson().fromJson(msg, FailureResponse.class);
        if(isViewAttached())
            this.getView().showFailure(failureResponse);
    }

}
