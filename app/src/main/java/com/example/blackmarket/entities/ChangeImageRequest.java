package com.example.blackmarket.entities;

public class ChangeImageRequest {
    /**
     * {
     *   "studentNumber": 201582322312,
     *   "url": "http://47.99.159.129:8081/dsdasfdashifweoihfhfiew.jpg"
     * }
     */

    private String studentNumber;
    private String url;

    public ChangeImageRequest(String studentNumber, String url) {
        this.studentNumber = studentNumber;
        this.url = url;
    }


    // Getter Methods

    public String getStudentNumber() {
        return studentNumber;
    }

    public String getUrl() {
        return url;
    }

    // Setter Methods

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
