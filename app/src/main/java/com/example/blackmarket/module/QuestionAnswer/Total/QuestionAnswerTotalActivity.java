package com.example.blackmarket.module.QuestionAnswer.Total;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.blackmarket.R;
import com.example.blackmarket.adapter.OrderAdapter;
import com.example.blackmarket.adapter.QuestionAdapter;
import com.example.blackmarket.entities.QuestionList;
import com.example.blackmarket.entities.SellingShopItem;
import com.example.blackmarket.entities.SpacesItemDecoration;
import com.example.blackmarket.module.Base.BaseActivity;
import com.example.blackmarket.module.QuestionAnswer.Posting.QuestionAnswerPostingActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class QuestionAnswerTotalActivity extends BaseActivity implements QuestionAnswerTotalView{
    private ImageButton question_btn_back;
    private ImageButton question_btn_add;
    private Button question_btn_price;
    private Button question_btn_time;

    private QuestionAnswerTotalPresenter questionAnswerTotalPresenter;
    private int pageNo=1;
    private static final int pageSize=20;//页大小
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private QuestionAdapter adapter;
    private List<QuestionList.Question> dataList=new ArrayList<>();
    private Context context;

    //标识排序
    int priceSort=0;
    int timeSort=0;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    //返回时可刷新数据
    @Override
    protected void onStart() {
        super.onStart();
        //获取数据
        getData(1,1);
    }


    private void getData(int pageno,int type){
        questionAnswerTotalPresenter.getData(pageno,type);
    }

    @Override
    protected void onBindView() {
        question_btn_back=findViewById(R.id.question_btn_back);
        question_btn_add=findViewById(R.id.question_btn_add);
        question_btn_price=findViewById(R.id.question_btn_price);
        question_btn_time=findViewById(R.id.question_btn_time);

        context=this;

        questionAnswerTotalPresenter=new QuestionAnswerTotalPresenter(this);
        recyclerView = findViewById(R.id.question_answer_total_recycler_view);
        layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        //设置recyclerview的item边距
        HashMap<String, Integer> spacesValue = new HashMap<>();
        spacesValue.put(SpacesItemDecoration.TOP_SPACE, 10);
        spacesValue.put(SpacesItemDecoration.BOTTOM_SPACE, 10);
        spacesValue.put(SpacesItemDecoration.LEFT_SPACE, 15);
        spacesValue.put(SpacesItemDecoration.RIGHT_SPACE, 15);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacesValue));

        adapter=new QuestionAdapter(dataList,this);

        adapter.setOnItemClickListener(new QuestionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                Toast.makeText(context, "Click "+ pos,Toast.LENGTH_SHORT).show();

            }
        });

        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void setButtonClick() {
        question_btn_back.setOnClickListener(this);
        question_btn_add.setOnClickListener(this);
        question_btn_price.setOnClickListener(this);
        question_btn_time.setOnClickListener(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.question_answer_total;
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.question_btn_back:{
                finish();
                break;
            }
            case R.id.question_btn_add:{
                Intent intent=new Intent(this, QuestionAnswerPostingActivity.class);
                startActivity(intent);
                break;
            }
            //按价格排序
            case R.id.question_btn_price:{
                if(priceSort==0){
                    dataList.clear();
                    getData(1,3);
                    question_btn_price.setText("按价格从大到小排");
                    priceSort=1;
                }else{
                    dataList.clear();
                    getData(1,2);
                    question_btn_price.setText("按价格从小到大排");
                    priceSort=0;
                }
                break;
            }
            //按时间排序
            case R.id.question_btn_time:{
                if(timeSort==0){
                    dataList.clear();
                    getData(1,5);
                    question_btn_time.setText("按时间从晚到早排");
                    timeSort=1;
                }else{
                    dataList.clear();
                    getData(1,4);
                    question_btn_time.setText("按时间从早到晚排");
                    timeSort=0;
                }

                break;
            }
            default:break;
        }
    }

    @Override
    public void showData(Object data) {
        QuestionList questionList=(QuestionList)data;
        List<QuestionList.Question> records=questionList.getRecords();
        if (pageNo==questionList.getCurrent()){
            dataList.addAll(records);
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void showToast(String msg) {

    }
}
