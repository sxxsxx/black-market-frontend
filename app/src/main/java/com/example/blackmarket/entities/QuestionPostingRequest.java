package com.example.blackmarket.entities;

public class QuestionPostingRequest {
    private String consultationDescription;
    private double consultationPrice;
    private String consultationTitle;

    public QuestionPostingRequest(String consultationDescription, double consultationPrice, String consultationTitle) {
        this.consultationDescription = consultationDescription;
        this.consultationPrice = consultationPrice;
        this.consultationTitle = consultationTitle;
    }

    public String getConsultationDescription() {
        return consultationDescription;
    }

    public void setConsultationDescription(String consultationDescription) {
        this.consultationDescription = consultationDescription;
    }

    public double getConsultationPrice() {
        return consultationPrice;
    }

    public void setConsultationPrice(double consultationPrice) {
        this.consultationPrice = consultationPrice;
    }

    public String getConsultationTitle() {
        return consultationTitle;
    }

    public void setConsultationTitle(String consultationTitle) {
        this.consultationTitle = consultationTitle;
    }
}
