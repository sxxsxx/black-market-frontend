package com.example.blackmarket.entities;

import java.util.List;

public class QuestionList implements Comparable<QuestionList.Question>{
    @Override
    public int compareTo(Question question) {
        return 0;
    }

    private int current;
    private List<Question> records;
    private boolean searchCount;
    private int size;
    private int total;

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public List<Question> getRecords() {
        return records;
    }

    public void setRecords(List<Question> records) {
        this.records = records;
    }

    public boolean isSearchCount() {
        return searchCount;
    }

    public void setSearchCount(boolean searchCount) {
        this.searchCount = searchCount;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * {
     *   "current": 0,
     *   "pages": 0,
     *   "records": [
     *     {
     *       "askTime": "2020-11-23T07:52:23.726Z",
     *       "consultationDescription": "string",
     *       "consultationId": 0,
     *       "consultationPrice": 0,
     *       "consultationTitle": "string",
     *       "nickname": "string",
     *       "pictureLocation": "string",
     *       "pictureUrl": "string",
     *       "question_status": 0
     *     }
     *   ],
     *   "searchCount": true,
     *   "size": 0,
     *   "total": 0
     * }
     */
    public static class Question {
        private String askTime;
        private String consultationDescription;
        private String consultationId;
        private double consultationPrice;
        private String consultationTitle;
        private String nickname;
        private String pictureLocation;
        private String pictureUrl;
        private int question_status;

        public String getAskTime() {
            return askTime;
        }

        public void setAskTime(String askTime) {
            this.askTime = askTime;
        }

        public String getConsultationDescription() {
            return consultationDescription;
        }

        public void setConsultationDescription(String consultationDescription) {
            this.consultationDescription = consultationDescription;
        }

        public String getConsultationId() {
            return consultationId;
        }

        public void setConsultationId(String consultationId) {
            this.consultationId = consultationId;
        }

        public double getConsultationPrice() {
            return consultationPrice;
        }

        public void setConsultationPrice(double consultationPrice) {
            this.consultationPrice = consultationPrice;
        }

        public String getConsultationTitle() {
            return consultationTitle;
        }

        public void setConsultationTitle(String consultationTitle) {
            this.consultationTitle = consultationTitle;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getPictureLocation() {
            return pictureLocation;
        }

        public void setPictureLocation(String pictureLocation) {
            this.pictureLocation = pictureLocation;
        }

        public String getPictureUrl() {
            return pictureUrl;
        }

        public void setPictureUrl(String pictureUrl) {
            this.pictureUrl = pictureUrl;
        }

        public int getQuestion_status() {
            return question_status;
        }

        public void setQuestion_status(int question_status) {
            this.question_status = question_status;
        }
    }
}
