package com.example.blackmarket.entities;

import java.util.List;

public class SellingShopItem implements Comparable<SellingShopItem.ShopData>{
    /**
     * {
     *   "current": 0,
     *   "pages": 0,
     *   "records": [
     *     {
     *       "productDescription": "string",
     *       "productId": 0,
     *       "productName": "string",
     *       "productPictureLocation": "string",
     *       "productPrice": 0,
     *       "sellTime": "2020-10-26T12:12:35.133Z",
     *       "sellmanId": "string"
     *     }
     *   ],
     *   "searchCount": true,
     *   "size": 0,
     *   "total": 0
     * }
     * 注意变量名与字段名必须一致
     */
    private int current;
    private List<ShopData> records;
    private boolean searchCount;
    private int size;
    private int total;

    public boolean isSearchCount() {
        return searchCount;
    }

    public int getSize() {
        return size;
    }

    public int getTotal() {
        return total;
    }

    public void setSearchCount(boolean searchCount) {
        this.searchCount = searchCount;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setList(List<ShopData> list) {
        this.records = list;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public List<ShopData> getData() {
        return records;
    }

    public int getCurrent() {
        return current;
    }

    @Override
    public int compareTo(ShopData shopData) {
        return 0;
    }

    public static class ShopData{
        private String sellmanId;
        private String nickname;
        private String pictureUrl;
        private int productId;
        private String productPictureLocation;
        private String productName;
        private double productPrice;
        private String productDescription;
        private String sellTime;
        private String campus;

        public String getCampus() {
            return campus;
        }

        public void setCampus(String campus) {
            this.campus = campus;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public void setSellmanId(String sellmanId) {
            this.sellmanId = sellmanId;
        }

        public void setProductPictureLocation(String productPictureLocation) {
            this.productPictureLocation = productPictureLocation;
        }

        public void setPictureUrl(String pictureUrl) {
            this.pictureUrl = pictureUrl;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public void setProductPrice(double productPrice) {
            this.productPrice = productPrice;
        }

        public  void setNickname(String name){this.nickname = name;}

        public void setProductDescription(String productDescription){ this.productDescription = productDescription;}

        public void setSellTime(String sellTime) {
            this.sellTime = sellTime;
        }

        public int getProductId() {
            return productId;
        }

        public String getSellmanId() {
            return sellmanId;
        }

        public String getProductPictureLocation() {
            return productPictureLocation;
        }

        public String getPictureUrl() {
            return pictureUrl;
        }

        public String getProductName() {
            return productName;
        }

        public double getProductPrice() {
            return productPrice;
        }

        public String getNickname(){return nickname;}

        public String getProductDescription(){return productDescription;}

        public String getSellTime() {
            return sellTime;
        }
    }
}
