package com.example.blackmarket.entities;

import java.util.ArrayList;

/**
 * @ProjectName: blackmarket$
 * @Package: com.example.blackmarket.entities$
 * @ClassName: DoRunningQuery$
 * @Description: java类作用描述
 * @Author: yh
 * @CreateDate: 2020/11/24$ 17:32$
 * @UpdateUser: 更新者：
 * @UpdateDate: 2020/11/24$ 17:32$
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */

public class DoRunningQuery {
    private ArrayList<DoRunningItem> records;
    private int total;
    private int size;
    private int current;
    private ArrayList<DoRunningItem> orders;
    private boolean searchCount;
    private int pages;

    public ArrayList<DoRunningItem> getRecords() {
        return records;
    }

    public void setRecords(ArrayList<DoRunningItem> records) {
        this.records = records;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public ArrayList<DoRunningItem> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<DoRunningItem> orders) {
        this.orders = orders;
    }

    public boolean isSearchCount() {
        return searchCount;
    }

    public void setSearchCount(boolean searchCount) {
        this.searchCount = searchCount;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }




}
