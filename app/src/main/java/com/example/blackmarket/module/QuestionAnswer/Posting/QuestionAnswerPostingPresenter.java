package com.example.blackmarket.module.QuestionAnswer.Posting;

import android.os.Handler;
import android.util.Log;

import com.example.blackmarket.callback.IShopCallback;
import com.example.blackmarket.entities.SuccessResponse;
import com.example.blackmarket.model.BlackMarketApi;
import com.example.blackmarket.module.Base.BasePresenter;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class QuestionAnswerPostingPresenter extends BasePresenter<QuestionAnswerPostingView> implements IShopCallback {
    private Handler mHandler;
    public QuestionAnswerPostingPresenter(QuestionAnswerPostingView view) {
        attachView(view);
        mHandler=new Handler();
    }

    public void question_posting(String consultationDescription,double consultationPrice,String consultationTitle){
        BlackMarketApi.question_posting(consultationDescription, consultationPrice, consultationTitle, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    String data = response.body().string();
                    mHandler.post(() -> onSuccess(data));
                }
                else {
                    assert response.body() != null;
                    String msg = "code: "+ response.code() + " body: " +response.body().string();
                    Log.e("response failure",msg);
                    mHandler.post(()->onFail(msg));
                }
            }
        });
    }

    @Override
    public void onSuccess(Object data) {
        SuccessResponse successResponse = new Gson().fromJson(((String)data), SuccessResponse.class);
        this.getView().postSuccess(successResponse);
    }

    @Override
    public void onFail(String msg) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onComplete() {

    }


}
