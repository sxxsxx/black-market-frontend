package com.example.blackmarket.model;

import android.util.Log;

import com.example.blackmarket.configration.Config;
import com.example.blackmarket.entities.ChangeImageRequest;
import com.example.blackmarket.entities.ChangePersonalInfoRequest;
import com.example.blackmarket.entities.DoRunningItem;
import com.example.blackmarket.entities.PostingDataRequest;
import com.example.blackmarket.entities.PostingDataRequest1;
import com.example.blackmarket.entities.QuestionPostingRequest;
import com.example.blackmarket.entities.RegisterRequest;
import com.example.blackmarket.module.Base.BaseApplication;
import com.example.blackmarket.utils.OkHttpClientUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.Map;

import okhttp3.Callback;

/**
 * 将所有网络请求封装在一个类中，对于上层就像在调用api获取数据一样
 * 参考 https://gitee.com/oschina/android-app/blob/v4.1.7/app/src/main/java/net/oschina/app/api/remote/OSChinaApi.java
 */
public class BlackMarketApi {

    public static void login(String studentNumber, String password, Callback callback){
        //如果账号密码要放进requestBody里面就存到Map里作为第四个参数
        OkHttpClientUtil.getInstance().POST(Config.getUrl() + "/auth/" + studentNumber + "/" + password, callback, null, null);
    }

    public static void getVerifyCode(String mail, Callback callback) {
        OkHttpClientUtil.getInstance().POST(Config.getUrl()+ "/mail/validateCode/" + mail, callback,null, (JsonObject) null);
    }

    public static void register(String studentNumber, String password, String confirmPassword, String mail, String verifyCode, Callback callback) {
        if (!password.equals(confirmPassword)) {
            Log.d("error","两次密码不一致");
        }
        Gson gson = new Gson();
        RegisterRequest registerRequest = new RegisterRequest(studentNumber, password, mail, verifyCode);
        String registerRequestJson = gson.toJson(registerRequest);
        OkHttpClientUtil.getInstance().POST(Config.getUrl() + "/api/personal/register",callback,null,registerRequestJson);

    }

    public static void changePersonalInfo(String studentNumber, String nickname, String address, String campus, Callback callback){
        Gson gson = new Gson();
        ChangePersonalInfoRequest request = new ChangePersonalInfoRequest(address,nickname,studentNumber,campus);
        String requestJson = gson.toJson(request);
        OkHttpClientUtil.getInstance().POST(Config.getUrl() + "/api/personal/changeNameAndAddress",callback,null,requestJson);

    }

    //上传图片
    public static void upLoadImage(String imagePath, Callback callback){
        try {
            String studentNumber = BaseApplication.get("studentNumber", null);
            String url =  OkHttpClientUtil.getInstance().syncPostFile(Config.getUrl() + "/upload/image", imagePath);
            ChangeImageRequest changeImageRequest = new ChangeImageRequest(studentNumber, url);
            Gson gson = new Gson();
            String changeImageRequestJson = gson.toJson(changeImageRequest);
            OkHttpClientUtil.getInstance().POST(Config.getUrl() + "/api/personal/changePersonalImage", callback, null, changeImageRequestJson);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void getShopListData(final String viewName, final int pageNo, final int typeId, final Callback callBack) {
        String url;
        OkHttpClientUtil okHttpClientUtil = OkHttpClientUtil.getInstance();
        switch (viewName){
            case "f1":
                url = Config.getUrl() + "/api/productTrade/queryByPage/"+pageNo+"/20/"+typeId;
                okHttpClientUtil.GET(url,callBack);
//                Log.d("getShopListData","get f1 data");
                break;
            case "f2":
                url = Config.getUrl() + "/api/productTrade/queryShoppingGuideByPage/" + pageNo + "/20/"+ typeId;
                okHttpClientUtil.GET(url, callBack);
//                Log.d("getShopListData","get f2 data");
                break;
            default:
                Log.e("getShopListData","请求失败：页面名有误");
        }
    }

    public static void getPersonalData(final Callback callback){
        OkHttpClientUtil okHttpClientUtil = OkHttpClientUtil.getInstance();
        String url = Config.getUrl() + "/api/personal/getPersonalInfo";
        okHttpClientUtil.GET(url, callback);
    }

    public static void getOrderList(final Callback callback){
        OkHttpClientUtil.getInstance().GET(Config.getUrl() + "/api/personal/QueryHistoricalOrder", callback);
    }

    public static void ItemDetailRequest(String seller_id, String buyer_id, int item_id, int type, final Callback itemDetailCallback) {
        //1是购买，2是售出
        if(type == 1){
            String url = Config.getUrl() +"/api/productTrade/purchase/" + item_id + "/" + buyer_id + "/" + seller_id;
            OkHttpClientUtil.getInstance().GET(url, itemDetailCallback);
        }else{
            String url = Config.getUrl() + "/api/productTrade/sell/" + item_id;
            OkHttpClientUtil.getInstance().GET(url,itemDetailCallback);
        }
    }
    //查询付费咨询
    public static void question_total(int pageNo,int typeId,final Callback QuestionTotalCallback){
        String url=Config.getUrl()+"/api/consultation/queryConsultationByPage/"+pageNo+"/20/"+typeId;
        OkHttpClientUtil.getInstance().GET(url,QuestionTotalCallback);
    }
    //发布付费咨询
    public static void question_posting(String consultationDescription,double consultationPrice,String consultationTitle,final Callback QuestionPostingCallback){
        Log.d("request","发布付费咨询");
        Gson gson=new Gson();
        QuestionPostingRequest questionPostingRequest=new QuestionPostingRequest(consultationDescription,consultationPrice,consultationTitle);
        String questionPostingRequestJson=gson.toJson(questionPostingRequest);
        OkHttpClientUtil.getInstance().POST(Config.getUrl()+"/api/consultation/upload",QuestionPostingCallback,null,questionPostingRequestJson);
    }

    //发布求购
    public static void postingData(String campus,String shoppingGuideBuyerId,String shoppingGuideDescription,String shoppingGuideName,String shoppingGuidePictureLocation,double shoppingGuidePrice,final Callback PSWcallback) {
        try {
            Log.d("request", "求购");
            String url = OkHttpClientUtil.getInstance().syncPostFile(Config.getUrl() + "/upload/image", shoppingGuidePictureLocation);
            Gson gson = new Gson();
            PostingDataRequest postingDataRequest = new PostingDataRequest(campus, shoppingGuideBuyerId, shoppingGuideDescription, shoppingGuideName, url, shoppingGuidePrice);
            String postingDataRequestJson = gson.toJson(postingDataRequest);
            OkHttpClientUtil.getInstance().POST(Config.getUrl() + "/api/productTrade/askForProduct", PSWcallback, null, postingDataRequestJson);

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    //发布出售
    public static void postingData1(String campus,String sellManId,String productDescription,String productName,String productPictureLocation,double productPrice,final Callback PSWcallback){

        try{
            Log.d("request","出售");
            String url=OkHttpClientUtil.getInstance().syncPostFile(Config.getUrl()+"/upload/image",productPictureLocation);
            Gson gson = new Gson();
            PostingDataRequest1 postingDataRequest1 = new PostingDataRequest1(campus,sellManId,productDescription,productName,url,productPrice);
            String postingDataRequestJson1 = gson.toJson(postingDataRequest1);
            OkHttpClientUtil.getInstance().POST(Config.getUrl() + "/api/productTrade/sellProduct",PSWcallback,null,postingDataRequestJson1);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    //获取学生用户ID
    public static void getStudentId(final Callback PSWcallback){
        String url = Config.getUrl() + "/api/personal/getPersonalInfo" ;
        OkHttpClientUtil.getInstance().GET(url, PSWcallback);
    }

    //发布代跑腿Post
    public static void posting_sell(String endPlace, String note,String price ,
                                    String proposerNumber,String startPlace ,String weight,
                                    Callback callback){
        Log.d("发布代跑腿request","#");
        Gson gson = new Gson();
        DoRunningItem doRunningItem = new DoRunningItem(endPlace,  note, price ,
                 proposerNumber, startPlace , weight);
        String doRunningItemRequestJson = gson.toJson(doRunningItem);
        Log.d("request body:",doRunningItemRequestJson);
        OkHttpClientUtil.getInstance().POST(Config.getUrl() + "/api/errandTrade/postErrand",
                callback,null,doRunningItemRequestJson);
        return;

    }

    //查看已发布的代跑腿任务
    public static void queryErrandTaskByPage(Callback callback){
        String url = Config.getUrl()+"/api/errandTrade/queryErrandTaskByPage/1/1000/1";
        OkHttpClientUtil.getInstance().GET(url,callback);

    }
    //接单代跑腿任务
    public static void acceptErrandTask(String errandID,String acceptorID,Callback callback){
        String url = Config.getUrl()+"/api/errandTrade/acceptErrand/"+errandID+"/"+
                acceptorID;
        OkHttpClientUtil.getInstance().GET(url,callback);
    }
    public static void confirmReceive(int productId, Callback callback) {
        OkHttpClientUtil.getInstance().GET(Config.getUrl() + "/api/productTrade/tradeFinished/" + productId, callback);
    }

    public static void recharge(float money, Callback callback){
        OkHttpClientUtil.getInstance().GET(Config.getUrl() + "/aliPay/Recharge/" + (String.valueOf(money)), callback);
    }
}
