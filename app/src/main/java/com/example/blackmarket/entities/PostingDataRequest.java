package com.example.blackmarket.entities;

public class PostingDataRequest {
    private String campus;
    private String shoppingGuideBuyerId;
    private String shoppingGuideDescription;
    private String shoppingGuideName;
    private String shoppingGuidePictureLocation;
    private double shoppingGuidePrice;

    public PostingDataRequest(String campus, String shoppingGuideBuyerId, String shoppingGuideDescription, String shoppingGuideName, String shoppingGuidePictureLocation, double shoppingGuidePrice) {
        this.campus = campus;
        this.shoppingGuideBuyerId = shoppingGuideBuyerId;
        this.shoppingGuideDescription = shoppingGuideDescription;
        this.shoppingGuideName = shoppingGuideName;
        this.shoppingGuidePictureLocation = shoppingGuidePictureLocation;
        this.shoppingGuidePrice = shoppingGuidePrice;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getShoppingGuideBuyerId() {
        return shoppingGuideBuyerId;
    }

    public void setShoppingGuideBuyerId(String shoppingGuideBuyerId) {
        this.shoppingGuideBuyerId = shoppingGuideBuyerId;
    }

    public String getShoppingGuideDescription() {
        return shoppingGuideDescription;
    }

    public void setShoppingGuideDescription(String shoppingGuideDescription) {
        this.shoppingGuideDescription = shoppingGuideDescription;
    }

    public String getShoppingGuideName() {
        return shoppingGuideName;
    }

    public void setShoppingGuideName(String shoppingGuideName) {
        this.shoppingGuideName = shoppingGuideName;
    }

    public String getShoppingGuidePictureLocation() {
        return shoppingGuidePictureLocation;
    }

    public void setShoppingGuidePictureLocation(String shoppingGuidePictureLocation) {
        this.shoppingGuidePictureLocation = shoppingGuidePictureLocation;
    }

    public double getShoppingGuidePrice() {
        return shoppingGuidePrice;
    }

    public void setShoppingGuidePrice(int shoppingGuidePrice) {
        this.shoppingGuidePrice = shoppingGuidePrice;
    }
}
