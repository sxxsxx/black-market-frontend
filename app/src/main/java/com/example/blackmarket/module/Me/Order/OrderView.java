package com.example.blackmarket.module.Me.Order;

public interface OrderView {
    void showData(Object data);
    void showFailure(String msg);
    void confirmSuccess();
}
