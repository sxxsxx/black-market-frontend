package com.example.blackmarket.module.QuestionAnswer.Posting;

public interface QuestionAnswerPostingView {
    //发布成功
    void postSuccess(Object data);

    //发布失败（输入信息不完整）
    void postFailure(String msg);
}
