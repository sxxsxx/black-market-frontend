package com.example.blackmarket.entities;

public class ChangePersonalInfoRequest {
    private String address;
    private String nickname;
    private String studentNumber;
    private String campus;
    public ChangePersonalInfoRequest(
            String address,
            String nickname,
            String studentNumber,
            String campus
    ){
        this.address = address;
        this.nickname = nickname;
        this.studentNumber =studentNumber;
        this.campus = campus;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public String getNickname() {
        return nickname;
    }

    public String getAddress() {
        return address;
    }

    public String getCampus() {
        return campus;
    }
}
