package com.example.blackmarket.entities;

public class UploadResponse {

    /**
     * {
     *   "describe": "null",
     *   "success": true,
     *   "url": "http://127.0.0.1:8081:35fewr23r23fwe32rs12fd.jpg"
     * }
     */

    private String describe;
    private boolean success;
    private String url;


    // Getter Methods

    public String getDescribe() {
        return describe;
    }

    public boolean getSuccess() {
        return success;
    }

    public String getUrl() {
        return url;
    }

    // Setter Methods

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
