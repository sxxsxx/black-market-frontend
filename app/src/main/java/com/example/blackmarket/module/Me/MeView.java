package com.example.blackmarket.module.Me;

public interface MeView {
    void getData();
    void showData(Object data);
    void changeImageSuccess();
}
