package com.example.blackmarket.module.SecondHandTrade.Shoplist;

public interface IShoplistView<T>{
    void getData();
    void showData(T data);
    void showToast(String msg);
}
