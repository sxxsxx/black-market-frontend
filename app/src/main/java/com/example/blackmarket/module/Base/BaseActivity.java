package com.example.blackmarket.module.Base;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;


public abstract class BaseActivity extends FragmentActivity implements View.OnClickListener{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    abstract protected void onBindView();

    abstract protected void setButtonClick();

    abstract protected int getLayout();

    protected void init(){
        setContentView(getLayout());//!!!非常重要，否则会产生空指针异常
        onBindView();
        setButtonClick();
    }


}
