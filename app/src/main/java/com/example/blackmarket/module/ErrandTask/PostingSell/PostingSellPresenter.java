package com.example.blackmarket.module.ErrandTask.PostingSell;

import android.os.Handler;
import android.util.Log;

import com.example.blackmarket.callback.IShopCallback;
import com.example.blackmarket.model.BlackMarketApi;
import com.example.blackmarket.module.Base.BasePresenter;
import com.example.blackmarket.module.RegisterAndLogin.LoginView;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * @ProjectName: blackmarket$
 * @Package: com.example.blackmarket.module.ErrandTask.PostingSell$
 * @ClassName: PostingSellPresenter$
 * @Description: java类作用描述
 * @Author: yh
 * @CreateDate: 2020/11/14$ 9:47$
 * @UpdateUser: 更新者：
 * @UpdateDate: 2020/11/14$ 9:47$
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */

public class PostingSellPresenter extends BasePresenter<LoginView> implements IShopCallback {
    private Handler mHandler=new Handler();


    public void posting_sell(String endPlace, String note,String price ,
                             String proposerNumber,String startPlace ,String weight){
        BlackMarketApi.posting_sell( endPlace,  note, price ,
                proposerNumber, startPlace, weight, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    String data = response.body().string();
                    mHandler.post(() -> onSuccess(data));
                    Log.d("post_sell",data);
                }
                else {
                    assert response.body() != null;
                    String msg = "code: "+ response.code() + " body: " +response.body().string();
                    Log.e("response failure",msg);
                    onFail(msg);
                }
            }
        }  );
    }

    @Override
    public void onSuccess(Object data) {

    }



    @Override
    public void onError() {

    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onFail(String msg) {
        // To do
        Log.d("error byg PostingSellPresenter.java, %s",msg);

    }


}
