package com.example.blackmarket.module.Home;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.utils.widget.ImageFilterView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.example.blackmarket.R;
import com.example.blackmarket.adapter.MainFragmentPagerAdapter;
import com.example.blackmarket.module.Base.BaseActivity;
import com.example.blackmarket.module.Me.MeFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    private ViewPager viewPager;
    private  List<Fragment> fragments;

    private FragmentManager mManager = getSupportFragmentManager();

    //底部四个tabbar的view
    private View home;
    private View web;
    private View message;
    private View me;

    private ImageFilterView home_icon_image;
    private ImageFilterView web_icon_image;
    private ImageFilterView message_icon_image;
    private ImageFilterView me_icon_image;

    private TextView home_icon_text;
    private TextView web_icon_text;
    private TextView message_icon_text;
    private TextView me_icon_text;

    @Override
    protected int getLayout() { return R.layout.activity_main; }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        this.viewPager = (ViewPager) findViewById(R.id.fragment_container_home);

        initViewPager();
    }
    @Override
    protected void onBindView() {
        home = findViewById(R.id.home);
        web = findViewById(R.id.web);
        message = findViewById(R.id.message);
        me = findViewById(R.id.me);

        home_icon_image=findViewById(R.id.imageView_home);
        web_icon_image=findViewById(R.id.imageView_web);
        message_icon_image=findViewById(R.id.imageView_message);
        me_icon_image=findViewById(R.id.imageView_me);
    }

    /**
     * 按钮设置监听器
     */
    @Override
    protected void setButtonClick() {
        home.setOnClickListener(this);
        web.setOnClickListener(this);
        message.setOnClickListener(this);
        me.setOnClickListener(this);

    }

    public void refresh(){
        home_icon_image.setImageResource(R.drawable.ic_baseline_home_stroke);
        web_icon_image.setImageResource(R.drawable.ic_baseline_web_stroke);
        message_icon_image.setImageResource(R.drawable.ic_baseline_message_stroke);
        me_icon_image.setImageResource(R.drawable.ic_baseline_person_stroke);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.home:{
                Log.d("home", "onClick: home");
                refresh();
                home_icon_image.setImageResource(R.drawable.ic_baseline_home_fill);
                viewPager.setCurrentItem(0);
                break;
            }
            case R.id.web:{
                refresh();
                web_icon_image.setImageResource(R.drawable.ic_baseline_web_fill);
                viewPager.setCurrentItem(1);
                break;
            }
            case R.id.message:{
                refresh();
                message_icon_image.setImageResource(R.drawable.ic_baseline_message_fill);
                viewPager.setCurrentItem(2);
                break;
            }
            case R.id.me:{
                refresh();
                me_icon_image.setImageResource(R.drawable.ic_baseline_person_fill);
                viewPager.setCurrentItem(3);
                break;
            }
        }
    }
    private void initViewPager(){
        fragments=new ArrayList<>();
        fragments.add(new HomeFragment());
        fragments.add(new WebFragment());
        fragments.add(new MessageFragment());
        fragments.add(new MeFragment());
        viewPager.setAdapter(new MainFragmentPagerAdapter(getSupportFragmentManager(),fragments));
        viewPager.addOnPageChangeListener(this);
        viewPager.setCurrentItem(0);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}