package com.example.blackmarket.entities;

public class JsonWebToken {
    private String token;


    // Getter Methods

    public String getToken() {
        return token;
    }

    // Setter Methods

    public void setToken(String token) {
        this.token = token;
    }
}
