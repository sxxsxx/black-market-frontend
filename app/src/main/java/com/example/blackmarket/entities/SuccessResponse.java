package com.example.blackmarket.entities;

public class SuccessResponse {

    /**
     * {
     *   "message": "请求成功",
     *   "status": 200,
     *   "success": true
     * }
     */
    private int status;
    private boolean success;
    private String message;

    // Getter Methods

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

    public boolean getSuccess() {
        return success;
    }

    // Setter Methods

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
