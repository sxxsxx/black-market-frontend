package com.example.blackmarket.module.Me.Order;

import android.os.Handler;
import android.util.Log;

import com.example.blackmarket.callback.IShopCallback;
import com.example.blackmarket.entities.OrderList;
import com.example.blackmarket.entities.SuccessResponse;
import com.example.blackmarket.model.BlackMarketApi;
import com.example.blackmarket.module.Base.BasePresenter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class OrderPresenter extends BasePresenter<OrderView> implements IShopCallback {
    private Handler mHandler;
    public OrderPresenter(OrderView view){
        attachView(view);
        mHandler=new Handler();
    }

    public void getData(){
        BlackMarketApi.getOrderList(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mHandler.post(()->onFail(e.toString()));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    String data = response.body().string();
                    mHandler.post(() -> onSuccess(data));
                }
                else {
                    assert response.body() != null;
                    String msg = "code: "+ response.code() + " body: " +response.body().string();
                    Log.e("response failure",msg);
                    mHandler.post(()->onFail(msg));
                }
            }
        });
    }

    public void confirm(int productId){
        BlackMarketApi.confirmReceive(productId, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mHandler.post(()->onFail(e.toString()));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    String data = response.body().string();
                    mHandler.post(() -> onConfirmSuccess(data));
                }
                else {
                    assert response.body() != null;
                    String msg = "code: "+ response.code() + " body: " +response.body().string();
                    Log.e("response failure",msg);
                    mHandler.post(()->onFail(msg));
                }
            }
        });
    }

    @Override
    public void onSuccess(Object data) {
        Log.e("data",data.toString());
        JsonParser parser = new JsonParser();
        JsonArray arr = parser.parse(data.toString()).getAsJsonArray();
        Gson gson = new Gson();
        List<OrderList.Order> orderList = new ArrayList<>();
        for(JsonElement ele: arr){
            OrderList.Order item = gson.fromJson(ele, OrderList.Order.class);
            orderList.add(item);
        }
        if(isViewAttached()){
            getView().showData(orderList);
        }
    }

    public void onConfirmSuccess(Object data){
        SuccessResponse response = new Gson().fromJson((String)data, SuccessResponse.class);
        if(response.getSuccess()){
            if(isViewAttached()){
                getView().confirmSuccess();
            }
        }else{
            if(isViewAttached()){
                getView().showFailure(response.getMessage());
            }
        }
    }

    @Override
    public void onFail(String msg) {
        if(isViewAttached()){
            getView().showFailure(msg);
        }
    }

    @Override
    public void onError() {

    }

    @Override
    public void onComplete() {

    }
}
