package com.example.blackmarket.entities;

import java.util.List;

public class OrderList implements  Comparable<OrderList.Order>{
    @Override
    public int compareTo(Order o) {
        return 0;
    }
    /**
    [
    {
        "buyTime": "2020-11-14T02:51:40.821637512",
            "productName": "书包",
            "productPictureLocation": "http://127.1.0.1:111111111111111111111111111111111.jpg",
            "productPrice": 10,
            "sellManId": 201830130301,
            "state": 1,
            "productId": 1
     }]
     **/
    private List<Order> records;

    public void setRecords(List<Order> records) {
        this.records = records;
    }

    public List<Order> getRecords() {
        return records;
    }

    public static class Order{
        private String buyTime;
        private String productName;
        private String productPictureLocation;
        private double productPrice;
        private String sellManId;
        private int state;
        private int productId;

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public void setBuyTime(String buyTime) {
            this.buyTime = buyTime;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public void setProductPictureLocation(String productPictureLocation) {
            this.productPictureLocation = productPictureLocation;
        }

        public void setProductPrice(double productPrice) {
            this.productPrice = productPrice;
        }

        public void setSellManId(String sellManId) {
            this.sellManId = sellManId;
        }

        public void setState(int state) {
            this.state = state;
        }

        public double getProductPrice() {
            return productPrice;
        }

        public int getProductId() {
            return productId;
        }

        public int getState() {
            return state;
        }

        public String getBuyTime() {
            return buyTime;
        }

        public String getProductName() {
            return productName;
        }

        public String getProductPictureLocation() {
            return productPictureLocation;
        }

        public String getSellManId() {
            return sellManId;
        }
    }
}
