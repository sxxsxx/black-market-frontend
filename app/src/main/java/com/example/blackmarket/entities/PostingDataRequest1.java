package com.example.blackmarket.entities;

public class PostingDataRequest1 {
    private String campus;
    private String productDescription;
    private String productName;
    private String productPictureLocation;
    private String sellManId;
    private double productPrice;

    public PostingDataRequest1(String campus, String sellManId, String productDescription, String productName, String productPictureLocation, double productPrice) {
        this.campus = campus;
        this.productDescription = productDescription;
        this.productName = productName;
        this.productPictureLocation = productPictureLocation;
        this.sellManId = sellManId;
        this.productPrice = productPrice;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPictureLocation() {
        return productPictureLocation;
    }

    public void setProductPictureLocation(String productPictureLocation) {
        this.productPictureLocation = productPictureLocation;
    }

    public String getSellManId() {
        return sellManId;
    }

    public void setSellManId(String sellManId) {
        this.sellManId = sellManId;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }
}
