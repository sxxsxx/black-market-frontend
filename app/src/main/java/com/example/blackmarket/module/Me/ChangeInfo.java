package com.example.blackmarket.module.Me;

public interface ChangeInfo {
    void showSuccess();
    void showFailure(String msg);
}
