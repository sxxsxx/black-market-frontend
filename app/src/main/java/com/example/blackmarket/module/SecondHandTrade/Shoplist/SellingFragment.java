package com.example.blackmarket.module.SecondHandTrade.Shoplist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.blackmarket.R;
import com.example.blackmarket.module.SecondHandTrade.ItemDetail.ItemDetailActivity;
import com.example.blackmarket.adapter.ShoplistAdpter;
import com.example.blackmarket.entities.SellingShopItem;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

public class SellingFragment extends Fragment implements IShoplistView {
    private ShoplistPresenter shoplistPresenter;
    private ShoplistAdpter adapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<SellingShopItem.ShopData> dataList = new ArrayList<>();
    private int pageNo=1;//当前数据加载到第几页
    private final int pageSize=20;
    private int typeId=1;//当前数据如何排序
    private int lastLoadDataItemPosition;//加载更多数据时最后一项是第几个数据
    private int listChange = 1;//判断其他页面回到当前页面时是否需要更新数据,数组为1是为了让第一次加载时也能更新数据
    static final int PICK_CONTACT_REQUEST = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shoplistPresenter = new ShoplistPresenter(SellingFragment.this);
    }

    @Override
    public void onDestroy() {
        shoplistPresenter.detachView();
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        @SuppressLint("ResourceType")
        View view = inflater.inflate(R.layout.fragment_selling,container,false);
        onBindView(view);
        //getData();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PICK_CONTACT_REQUEST && resultCode==RESULT_OK){//request 0判断是该页面打开的详情页面，result 1代表详情页面发生了购买
            listChange=1;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(listChange==1){
            dataList.clear();
            getData();
            listChange=0;//重置code避免重复更新数据
        }
    }

    protected void onBindView(View view){
        recyclerView = view.findViewById(R.id.recycler_sell);
        layoutManager = new GridLayoutManager(getContext(),2);//双列布局

        recyclerView.setLayoutManager(layoutManager);
        adapter = new ShoplistAdpter(dataList,getContext());

        //设置点击item后的动作，在这里设置如何进入商品详情页
        adapter.setOnItemClickListener(new ShoplistAdpter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //Toast.makeText(getContext(), "Click "+position,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), ItemDetailActivity.class);
                intent.putExtra("sellmanId",dataList.get(position).getSellmanId());
                intent.putExtra("username",dataList.get(position).getNickname());
                intent.putExtra("campus",dataList.get(position).getCampus());
                intent.putExtra("price",dataList.get(position).getProductPrice());
                intent.putExtra("Image",dataList.get(position).getPictureUrl());
                intent.putExtra("type",1);
                intent.putExtra("item_id",dataList.get(position).getProductId());
                intent.putExtra("areaNum",dataList.get(position).getCampus().equals("南校") ? 1 : 2);
                startActivityForResult(intent, PICK_CONTACT_REQUEST);
            }
        });
        setScrollNextPage();
        recyclerView.setAdapter(adapter);
    }

    /**
     * 让presenter去获取数据
     */
    @Override
    public void getData() {
        Log.d("SellingFragment pageNo", String.valueOf(pageNo));
        shoplistPresenter.getData(pageNo, typeId);
    }

    /**
     * 如果成功请求到数据，presenter会调用该接口显示数据
     * @param data
     */
    @Override
    public void showData(Object data) {
        SellingShopItem sellingShopItem = (SellingShopItem) data;
        dataList.addAll(sellingShopItem.getData());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showToast(String msg) {
        Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
    }

    protected void setScrollNextPage(){
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                /**
                 * 当下拉到当前列表最后一个商品时，请求并加载新的数据，届时可以通过item数量算出下一页的页码
                 */
                if (newState==SCROLL_STATE_IDLE && lastLoadDataItemPosition==adapter.getItemCount() && pageNo*pageSize==adapter.getItemCount()){
                    //在此加载下一页数据
                    ++pageNo;
                    Log.d("SellingFragment scroll", String.valueOf(pageNo));
                    getData();
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager instanceof LinearLayoutManager){
                    int firstVisibleItemPosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                    int lastVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                    lastLoadDataItemPosition = firstVisibleItemPosition+(lastVisibleItemPosition-firstVisibleItemPosition)+1;
                }
            }
        });
    }

    public void setTypeId(int typeId){
        if(typeId!=this.typeId) {
            this.typeId = typeId;
            dataList.clear();
            getData();
        }
    }
}
