package com.example.blackmarket.module.ErrandTask.DoRunning;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.blackmarket.R;
import com.example.blackmarket.adapter.DoRunningAdapter;
import com.example.blackmarket.entities.DoRunningItem;
import com.example.blackmarket.entities.DoRunningQuery;
import com.example.blackmarket.module.Base.BaseActivity;
import com.example.blackmarket.module.ErrandTask.PostingSell.PostingSell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @ProjectName: blackmarket$
 * @Package: com.example.blackmarket.module.ErrandTask.PostingSell$
 * @ClassName: PostingSellPresenter$
 * @Description: java类作用描述
 * @Author: 作者名
 * @CreateDate: 2020/11/14$ 9:47$
 * @UpdateUser: 更新者：
 * @UpdateDate: 2020/11/14$ 9:47$
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */

public class DoRunningActivity extends BaseActivity implements DoRunningView {

    private Button do_running_btn_composite;
    private Button do_running_btn_price;
    private Button do_running_btn_time;
    private Button do_running_btn_campus;
    private ImageButton do_running_btn_back;
    private ImageButton do_running_btn_wanna_post;

    public List<DoRunningItem> doRunningItemList ;

    private DoRunningPresenter doRunningPresenter;

    private RecyclerView recyclerView;

    @Override
    protected void onStart() {
        super.onStart();
        doRunningPresenter.queryErrandTaskByPage();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doRunningPresenter = new DoRunningPresenter(this);
        //doRunningPresenter.queryErrandTaskByPage();

        recyclerView = (RecyclerView)findViewById(R.id.do_running_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        doRunningItemList = new ArrayList<DoRunningItem>() ;
        DoRunningAdapter adapter = new DoRunningAdapter(doRunningItemList);
        recyclerView.setAdapter(adapter);




    }

    @Override
    protected void onBindView() {
        do_running_btn_composite=(Button)findViewById(R.id.do_running_btn_composite);
        do_running_btn_price=(Button)findViewById(R.id.do_running_btn_price);
        do_running_btn_time=(Button)findViewById(R.id.do_running_btn_time);
        do_running_btn_campus=(Button)findViewById(R.id.do_running_btn_campus);

        do_running_btn_back = findViewById(R.id.do_running_btn_back);
        do_running_btn_wanna_post=findViewById(R.id.do_running_btn_wanna_post);
    }

    @Override
    protected void setButtonClick() {
        do_running_btn_composite.setOnClickListener(this);
        do_running_btn_price.setOnClickListener(this);
        do_running_btn_time.setOnClickListener(this);
        do_running_btn_campus.setOnClickListener(this);

        do_running_btn_back.setOnClickListener(this);
        do_running_btn_wanna_post.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.do_running_btn_back:{
                finish();
                break;
            }
            case R.id.do_running_btn_wanna_post:{
                Intent intent2 = new Intent(this, PostingSell.class);
                startActivity(intent2);
                break;
            }
            default:{
                break;
            }
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.do_running_layout;
    }

    @Override
    public void queryErrandTaskByPageSuccess(Object data) {
        DoRunningQuery doRunningQuery = (DoRunningQuery)data;
        doRunningItemList = doRunningQuery.getRecords();
        Log.e("size:", (String.valueOf(doRunningItemList.size())));
        Collections.reverse(doRunningItemList);
        DoRunningAdapter adapter = new DoRunningAdapter(doRunningItemList);
        recyclerView.setAdapter(adapter);


    }

}

