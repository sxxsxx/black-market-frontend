package com.example.blackmarket.module.SecondHandTrade.Shoplist;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.example.blackmarket.R;
import com.example.blackmarket.adapter.ShopListFragmentPagerAdapter;
import com.example.blackmarket.module.Base.BaseActivity;
import com.example.blackmarket.module.SecondHandTrade.PostingSellOrWant.PostingSellOrWantActivity;

public class ShopListActivity extends BaseActivity implements ViewPager.OnPageChangeListener {
    private static final int PAGE_SELL=0;
    private static final int PAGE_BUY=1;
    private ViewPager viewPager;
    private ShopListFragmentPagerAdapter shopListFragmentPagerAdapter;
    private ImageButton btn_select;
    private Button btn_sell;
    private Button btn_wanna_buy;
    private ImageButton btn_back;
    private ImageButton btn_wanna_post;

    @Override
    protected int getLayout() { return R.layout.activity_shoplist; }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onBindView() {
        btn_select = findViewById(R.id.btn_select);
        btn_wanna_buy = findViewById(R.id.btn_wanna_buy);
        btn_sell = findViewById(R.id.btn_sell);
        btn_back = findViewById(R.id.btn_back);
        btn_wanna_post=findViewById(R.id.btn_wanna_post);

        viewPager = findViewById(R.id.fragment_container);
        shopListFragmentPagerAdapter = new ShopListFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(shopListFragmentPagerAdapter);
        viewPager.setCurrentItem(PAGE_SELL);
        viewPager.addOnPageChangeListener(this);
    }

    /**
     * 按钮设置监听器
     */
    @Override
    protected void setButtonClick() {
        btn_select.setOnClickListener(this);
        btn_sell.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        btn_wanna_buy.setOnClickListener(this);
        btn_wanna_post.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_back:{
                finish();
                break;
            }
            case R.id.btn_select:
                popUpMenu(view);
                break;
            case R.id.btn_sell:{
                viewPager.setCurrentItem(PAGE_SELL);
                break;
            }
            case R.id.btn_wanna_buy:{
                viewPager.setCurrentItem(PAGE_BUY);
                break;
            }
            case R.id.btn_wanna_post:{
                Intent intent2 = new Intent(this, PostingSellOrWantActivity.class);
                startActivity(intent2);
            }
        }
    }

    public void popUpMenu(View view){
        // View当前PopupMenu显示的相对View的位置
        PopupMenu popupMenu = new PopupMenu(this, view);
        // menu布局
        popupMenu.getMenuInflater().inflate(R.menu.shoplist_popupmenu, popupMenu.getMenu());
        // menu的item点击事件
        popupMenu.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()){
                case R.id.sort_default1:
                    setFragmentTypeId(1);
                    return false;
                case R.id.sort_south2:
                    setFragmentTypeId(2);
                    return false;
                case R.id.sort_north3:
                    setFragmentTypeId(3);
                    return false;
                case R.id.sort_up4:
                    setFragmentTypeId(4);
                    return false;
                case R.id.sort_down5:
                    setFragmentTypeId(5);
                    return false;
                default:
                    return false;
            }
        });

        popupMenu.show();
    }

    public void setFragmentTypeId(int typeId){
        SellingFragment sellingFragment = (SellingFragment) shopListFragmentPagerAdapter.getItem(0);
        sellingFragment.setTypeId(typeId);
        BuyingFragment buyingFragment = (BuyingFragment) shopListFragmentPagerAdapter.getItem(1);
        buyingFragment.setTypeId(typeId);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
        //state的状态有三个，0表示什么都没做，1正在滑动，2滑动完毕
        if (state == 2) {
            switch (viewPager.getCurrentItem()) {
                case PAGE_SELL:
                    onClick(btn_sell);
                    break;
                case PAGE_BUY:
                    onClick(btn_wanna_buy);
                    break;
            }
        }
    }
}
