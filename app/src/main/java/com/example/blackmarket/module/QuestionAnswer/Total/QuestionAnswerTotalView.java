package com.example.blackmarket.module.QuestionAnswer.Total;

public interface QuestionAnswerTotalView {
    void showData(Object data);
    void showToast(String msg);
}
