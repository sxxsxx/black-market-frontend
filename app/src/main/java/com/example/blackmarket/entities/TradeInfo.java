package com.example.blackmarket.entities;

public class TradeInfo {
    /**
     * {
     *   "response": "string"
     * }
     */

    private String response;


    // Getter Methods

    public String getResponse() {
        return response;
    }

    // Setter Methods

    public void setResponse(String response) {
        this.response = response;
    }
}
