package com.example.blackmarket.module.Me;

import android.os.Handler;
import android.util.Log;

import com.example.blackmarket.callback.IShopCallback;
import com.example.blackmarket.entities.SuccessResponse;
import com.example.blackmarket.entities.User;
import com.example.blackmarket.model.BlackMarketApi;
import com.example.blackmarket.module.Base.BasePresenter;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MePresenter extends BasePresenter<MeView> implements IShopCallback{
    private Handler mHandler;
    public MePresenter(MeView view) {
        attachView(view);
        mHandler=new Handler();
    }

    public void getData(){
        BlackMarketApi.getPersonalData(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    String data = response.body().string();
                    mHandler.post(() -> onSuccess(data));
                }
                else {
                    assert response.body() != null;
                    String msg = "code: "+ response.code() + " body: " +response.body().string();
                    Log.e("response failure",msg);
                    mHandler.post(()->onFail(msg));
                }
            }
        });
    }

    public void upload(String imagePath){
        BlackMarketApi.upLoadImage(imagePath, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFail(e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    String data = response.body().string();
                    mHandler.post(() -> onUploadSuccess(data));
                }
                else {
                    assert response.body() != null;
                    String msg = "code: "+ response.code() + " body: " +response.body().string();
                    Log.e("response failure",msg);
                    mHandler.post(()->onFail(msg));
                }
            }
        });
    }

    @Override
    public void onSuccess(Object data) {
        Log.d("mePresenter",data.toString());
        if(isViewAttached()){
            User user = new Gson().fromJson((String) data,User.class);
            this.getView().showData(user);
        }
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onError() {

    }

    public void onUploadSuccess(Object data) {
        if (isViewAttached()) {
            SuccessResponse successResponse = new Gson().fromJson((String) data, SuccessResponse.class);
            if (successResponse.getSuccess()) {
                this.getData();
                getView().changeImageSuccess();
            }
        }
    }

    @Override
    public void onFail(String msg) {
        Log.e("request error", msg);
    }
}
