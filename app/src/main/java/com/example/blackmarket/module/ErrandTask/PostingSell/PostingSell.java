package com.example.blackmarket.module.ErrandTask.PostingSell;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.Nullable;

import com.example.blackmarket.R;
import com.example.blackmarket.module.Base.BaseActivity;
import com.example.blackmarket.module.Base.BaseApplication;
import com.example.blackmarket.module.RegisterAndLogin.LoginView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @ProjectName: blackmarket$
 * @Package: com.example.blackmarket.module.ErrandTask.PostingSell$
 * @ClassName: PostingSellPresenter$
 * @Description: java类作用描述
 * @Author: 作者名
 * @CreateDate: 2020/11/14$ 9:47$
 * @UpdateUser: 更新者：
 * @UpdateDate: 2020/11/14$ 9:47$
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */

public class PostingSell extends BaseActivity  {
    private ImageButton posting_sell_back;
    private Button posting_sell_publish;

    private EditText posting_sell_price;
    private EditText posting_sell_src_place;
    private EditText posting_sell_dest_place;
    private EditText posting_sell_weight;
    private EditText posting_sell_ddl;
    private EditText posting_sell_additional_msg;

    private PostingSellPresenter postingSellPresenter;
    public void func(){
        
            System.out.println("thread id is: " + Thread.currentThread().getId());
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postingSellPresenter = new PostingSellPresenter();

    }

    @Override
    protected void onBindView() {
        posting_sell_back=(ImageButton) findViewById(R.id.posting_sell_back);
        posting_sell_publish=(Button)findViewById(R.id.posting_sell_publish);

        //下面这些和后端交互, 将会通过点击发布按钮上传到数据库
        posting_sell_price = findViewById(R.id.posting_sell_price);
        posting_sell_src_place = findViewById(R.id.posting_sell_src_place);
        posting_sell_dest_place = findViewById(R.id.posting_sell_dest_place);
        posting_sell_weight = findViewById(R.id.posting_sell_weight);
        posting_sell_ddl = findViewById(R.id.posting_sell_ddl);
        posting_sell_additional_msg = findViewById(R.id.posting_sell_additional_msg);

    }

    @Override
    protected void setButtonClick() {
        posting_sell_back.setOnClickListener(this);
        posting_sell_publish.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.posting_sell_back:{
                finish();
                break;
            }
            case R.id.posting_sell_publish:{
                postingSellPresenter.posting_sell(posting_sell_dest_place.getText().toString(),
                        posting_sell_additional_msg.getText().toString(),
                        posting_sell_price.getText().toString(),
                        BaseApplication.get("studentNumber","1"),
                        posting_sell_src_place.getText().toString(),
                        posting_sell_weight.getText().toString()
                        );
                //Log.d("click event on the button",posting_sell_dest_place.getText().toString());
                finish();
                break;
            }
            default:{
                //                ExecutorService executor = Executors.newFixedThreadPool(100000);
//                for (int i = 0; i < 100000; i++) {
//
//                    executor.execute(()->func());
//                }
            }

        }
    }

    @Override
    protected int getLayout() {
        return R.layout.posting_sell_layout;
    }
}

