package com.example.blackmarket.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.example.blackmarket.R;
import com.example.blackmarket.entities.DoRunningItem;
import com.example.blackmarket.entities.RecommendData;
import com.example.blackmarket.module.ErrandTask.RunningDetail.RunningDetailActivity;

import java.util.List;

/**
 * @ProjectName: blackmarket$
 * @Package: com.example.blackmarket.adapter$
 * @ClassName: DoRunningAdapter$
 * @Description: java类作用描述
 * @Author: yh
 * @CreateDate: 2020/11/14$ 10:51$
 * @UpdateUser: 更新者：
 * @UpdateDate: 2020/11/14$ 10:51$
 * @UpdateRemark: 更新说明：
 * @Version: 1.0
 */

public class DoRunningAdapter extends RecyclerView.Adapter<DoRunningAdapter.ViewHolder>{
    private List<DoRunningItem> mDoRunningItemList;
    private Context mContext;
    static class ViewHolder extends RecyclerView.ViewHolder{
        View DoRunningItemView;
        ImageView DoRunningItemImageView;
        TextView nicknameText;
        TextView startPlace;
        TextView endPlace;
        TextView time;
        TextView weight;
        TextView campus;
        TextView price;

        //构造函数
        //要传入View参数，通常就是RecyclerView子项的最外层布局
        public ViewHolder(View view){
            super(view);
            DoRunningItemView=view;
            DoRunningItemImageView=(ImageView)view.findViewById(R.id.do_running_user_image);
            nicknameText = view.findViewById(R.id.nickname);
            startPlace = view.findViewById(R.id.startPlace);
            endPlace = view.findViewById(R.id.endPlace);
            time = view.findViewById(R.id.time);
            weight = view.findViewById(R.id.weight);
            campus = view.findViewById(R.id.do_running_campus);
            price = view.findViewById(R.id.price);
        }
    }


    public DoRunningAdapter(List<DoRunningItem> doRunningItemList){
        mDoRunningItemList=doRunningItemList;
    }

    //用于创建ViewHolder实例
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.do_running_item,parent,false);
        this.mContext = parent.getContext();
        final ViewHolder holder=new ViewHolder(view);

        //首页分类图标的点击事件
        holder.DoRunningItemView.setOnClickListener((new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int position=holder.getAdapterPosition();
                DoRunningItem doRunningItem=mDoRunningItemList.get(position);
                Intent intent = new Intent(mContext,RunningDetailActivity.class);
                /*"campus": "南校",
                  "endPlace": "c10宿舍",
                  "endTime": "2020/10/9/10:21",
                  "errandId": 111,
                  "nickname": "xc",
                  "note": "这东西又重又大",
                  "pictureAddress": 10,
                  "price": 10,
                  "startPlace": "c10宿舍",
                  "weight": 10
                * */
                intent.putExtra("campus",doRunningItem.getCampus());
                intent.putExtra("endPlace",doRunningItem.getendPlace());
                intent.putExtra("endTime",doRunningItem.getEndTime());
                intent.putExtra("errandID",doRunningItem.getErrandId());
                intent.putExtra("nickname",doRunningItem.getNickname());
                intent.putExtra("note",doRunningItem.getnote());
                intent.putExtra("price",doRunningItem.getprice());
                intent.putExtra("startPlace",doRunningItem.getprice());
                intent.putExtra("weight",doRunningItem.getweight());
                intent.putExtra("user_image",doRunningItem.getPictureAddress());
//                intent.putExtra("proposerNumber",doRunningItem.getProposerNumber());
                mContext.startActivity(intent);
            }
        }));
        return holder;
    }

    //对RecyclerView子项数据进行赋值，会在每个子项被滚动到屏幕内时被执行
    //通过position得到当前项的Fruit实例，然后再将数据设置到ViewHolder的ImageView和TextView中
    @Override
    public void onBindViewHolder(ViewHolder holder,int position){
        DoRunningItem doRunningItem=mDoRunningItemList.get(position);
        Glide.with(mContext).load(doRunningItem.getPictureAddress()).apply(RequestOptions.bitmapTransform(new CircleCrop())).into(holder.DoRunningItemImageView);
        holder.nicknameText.setText(doRunningItem.getNickname());
        holder.campus.setText(doRunningItem.getCampus());
        holder.startPlace.setText("代拿地点："+doRunningItem.getstartPlace());
        holder.endPlace.setText("目的地点："+doRunningItem.getendPlace());
        holder.time.setText("截止时间："+doRunningItem.getEndTime());
        holder.weight.setText("重        量："+doRunningItem.getweight());
        holder.price.setText(doRunningItem.getprice()+"黑币");
    }

    @Override
    public int getItemCount(){
        return mDoRunningItemList.size();
    }
}